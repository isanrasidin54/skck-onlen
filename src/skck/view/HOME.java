/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import skck.koneksi.koneksi;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.text.View;
import skck.controller.ciri_fisik_controller;
import skck.controller.data_pribadi_controller;
import skck.controller.hubungan_keluarga_controller;
import skck.controller.keterangan_controller;
import skck.controller.perkara_pidana_controller;
import skck.controller.ciri_fisik_controller;
import skck.controller.lampiran_controller;
import skck.controller.pendidikan_controller;
import skck.controller.satuan_wilayah_controller;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JTabbedPane;
/**
 *
 * @author ALFINA
 */
public class HOME extends javax.swing.JFrame {
    private satuan_wilayah_controller sw;
    private data_pribadi_controller dp;
    private hubungan_keluarga_controller hk;
    private pendidikan_controller pk;
    private perkara_pidana_controller pp;
    private ciri_fisik_controller cf;
    private lampiran_controller lp;
    private keterangan_controller kt;
    
    private JFileChooser ktp = new JFileChooser();
    private JFileChooser paspor = new JFileChooser();
    private JFileChooser kk = new JFileChooser();
    private JFileChooser akte = new JFileChooser();
    private JFileChooser sidik = new JFileChooser();
    
    JTabbedPane jtp = new JTabbedPane();
    
    
    public void tampil_dp_provinsi()
        {
            try {
            Connection con = koneksi.getKoneksi();
            Statement stt = con.createStatement();
            String sql = "select nama from provinsi";      // disini saya menampilkan NIM, anda dapat menampilkan
            ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan

            while(res.next()){
                Object[] ob = new Object[3];
                ob[0] = res.getString(1);

                dp_cb_provinsi.addItem((String) ob[0]);                                      // fungsi ini bertugas menampung isi dari database
                hk_cb_provinsi.addItem((String) ob[0]);  
                hk_cb_provinsiA.addItem((String) ob[0]); 
                hk_cb_provinsiI.addItem((String) ob[0]); 
                pk_cb_provinsi.addItem((String) ob[0]); 
                sw_cb_provinsi.addItem((String) ob[0]); 
            }
            res.close(); stt.close();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }    
    
    public void tampil_dp_kabupaten()
        {
            try {
            Connection con = koneksi.getKoneksi();
            Statement stt = con.createStatement();
            String sql = "select nama from kabupaten";      // disini saya menampilkan NIM, anda dapat menampilkan
            ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan

            while(res.next()){
                Object[] ob = new Object[3];
                ob[0] = res.getString(1);

                dp_cb_kabupaten.addItem((String) ob[0]);
                hk_cb_kabupaten.addItem((String) ob[0]);
                hk_cb_kabupatenA.addItem((String) ob[0]);
                hk_cb_kabupatenI.addItem((String) ob[0]);
                pk_cb_kabupaten.addItem((String) ob[0]);
                sw_cb_kabupaten.addItem((String) ob[0]);
                // fungsi ini bertugas menampung isi dari database
            }
            res.close(); stt.close();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } 
    
    public void tampil_dp_kecamatan()
        {
            try {
            Connection con = koneksi.getKoneksi();
            Statement stt = con.createStatement();
            String sql = "select nama from kecamatan";      // disini saya menampilkan NIM, anda dapat menampilkan
            ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan

            while(res.next()){
                Object[] ob = new Object[3];
                ob[0] = res.getString(1);

                dp_cb_kecamatan.addItem((String) ob[0]);
                hk_cb_kecamatan.addItem((String) ob[0]);
                hk_cb_kecamatanA.addItem((String) ob[0]);
                hk_cb_kecamatanI.addItem((String) ob[0]);
                sw_cb_kecamatan.addItem((String) ob[0]);
            }
            res.close(); stt.close();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } 
    
    public void tampil_dp_kelurahan()
        {
            try {
            Connection con = koneksi.getKoneksi();
            Statement stt = con.createStatement();
            String sql = "select nama from kelurahan";      // disini saya menampilkan NIM, anda dapat menampilkan
            ResultSet res = stt.executeQuery(sql);                                // yang anda ingin kan

            while(res.next()){
                Object[] ob = new Object[3];
                ob[0] = res.getString(1);

                dp_cb_kelurahan.addItem((String) ob[0]);
                hk_cb_kelurahan.addItem((String) ob[0]);
                hk_cb_kelurahanA.addItem((String) ob[0]);
                hk_cb_kelurahanI.addItem((String) ob[0]);
                sw_cb_kelurahan.addItem((String) ob[0]);// fungsi ini bertugas menampung isi dari database
            }
            res.close(); stt.close();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } 
   

    //Satuan Wilayah
    public JComboBox getKeperluanSW(){
        return sw_cb_keperluan;
    }
    public JComboBox getKesatuanSW(){
        return sw_cb_kesatuan;
    }
    public JComboBox getTingkatSW(){
        return sw_cb_tingkat;
    }
    public JTextArea getAlamatSW(){
        return sw_tf_alamat;
    }
    public JComboBox getProvinsiSW(){
        return sw_cb_provinsi;
    }
    public JComboBox getKabupatenSW(){
        return sw_cb_kabupaten;
    }
    public JComboBox getKecamatanSW(){
        return sw_cb_kecamatan;
    }
    public JComboBox getKelurahanSW(){
        return sw_cb_kelurahan;
    }
    public JRadioButton getTunaiSW(){
        return sw_rb_tunai;
    }
    public JRadioButton getBrivaSW(){
        return sw_rb_briva;
    }
    public JTextField getNoRekSW(){
        return sw_tf_namarek;
    }
    
    //Data Pribadi 
    public JTextField getNamaDP(){
        return dp_tf_nama;
    }
    public JTextField getTempatDP(){
        return dp_tf_tempat;
    }
    public JTextField getTanggalDP(){
        return dp_tf_tanggal;
    }
    public JRadioButton getLakiDP(){
        return dp_rb_laki;
    }
    public JRadioButton getPerempuanDP(){
        return dp_rb_perempuan;
    }
    public JRadioButton getKawinDP(){
        return dp_rb_kawin;
    }
    public JRadioButton getBelumDP(){
        return dp_rb_belum;
    }
    public JRadioButton getWniDP(){
        return dp_rb_wni;
    }
    public JRadioButton getWnaDP(){
        return dp_rb_wna;
    }
    public JComboBox getAgamaDP(){
        return dp_cb_agama;
    }
    public JTextField getPekerjaanDP(){
        return dp_tf_pekerjaan;
    }
    public JTextField getNoTelpDP(){
        return dp_tf_notelp;
    }
    public JTextArea getAlamatDP(){
        return dp_tf_alamat;
    }
    public JComboBox getProvinsiDP(){
        return dp_cb_provinsi;
    }
    public JComboBox getKabupatenDP(){
        return dp_cb_kabupaten;
    }
    public JComboBox getKecamatanDP(){
        return dp_cb_kecamatan;
    }
    public JComboBox getKelurahanDP(){
        return dp_cb_kelurahan;
    }
    public JComboBox getTipeIdentitasDP(){
        return dp_cb_identitas;
    }
    public JTextField getNoIdentitasDP(){
        return dp_tf_noIdentitas;
    }
    
    //Hubungan Keluraga
    public JComboBox getHubunganHK(){
        return hk_cb_hubungan;
    }
    public JTextField getNamaHK(){
        return hk_tf_nama;
    }
    public JTextField getUmurHK(){
        return hk_tf_umur;
    }
    public JComboBox getAgamaHK(){
        return hk_cb_agama;
    }
    public JRadioButton getWniHK(){
        return hk_rb_wni;
    }
    public JRadioButton getWnaHK(){
        return hk_rb_wna;
    }
    public JTextArea getAlamatHK(){
        return hk_tf_alamat;
    }
    public JTextField getPekerjaanHK(){
        return hk_tf_pekerjaan;
    }
    public JComboBox getProvinsiHK(){
        return hk_cb_provinsi;
    }
    public JComboBox getKabupatenHK(){
        return hk_cb_kabupaten;
    }
    public JComboBox getKecamatanHK(){
        return hk_cb_kecamatan;
    }
    public JComboBox getKelurahanHK(){
        return hk_cb_kelurahan;
    }
    public JTextField getNamaAHK(){
        return hk_tf_namaA;
    }
    public JTextField getUmurAHK(){
        return hk_tf_umurA;
    }
    public JComboBox getAgamaAHK(){
        return hk_cb_agamaA;
    }
    public JRadioButton getWniAHK(){
        return hk_rb_wniA;
    }
    public JRadioButton getWnaAHK(){
        return hk_rb_wnaA;
    }
    public JTextField getPekerjaanAHK(){
        return hk_tf_pekerjaanA;
    }
    public JTextArea getAlamatAHK(){
        return hk_tf_alamatA;
    }
    public JComboBox getProvinsiAHK(){
        return hk_cb_provinsiA;
    }
    public JComboBox getKabupatenAHK(){
        return hk_cb_kabupatenA;
    }
    public JComboBox getKecamatanAHK(){
        return hk_cb_kecamatanA;
    }
    public JComboBox getKelurahanAHK(){
        return hk_cb_kelurahanA;
    }
    public JTextField getNamaIHK(){
        return hk_tf_namaI;
    }
    public JTextField getUmurIHK(){
        return hk_tf_umurI;
    }
    public JComboBox getAgamaIHK(){
        return hk_cb_agamaI;
    }
    public JRadioButton getWniIHK(){
        return hk_rb_wniI;
    }
    public JRadioButton getWnaIHK(){
        return hk_rb_wnaI;
    }
    public JTextField getPekerjaanIHK(){
        return hk_tf_pekerjaanI;
    }
    public JTextArea getAlamatIHK(){
        return hk_tf_alamatI;
    }
    public JComboBox getProvinsiIHK(){
        return hk_cb_provinsiI;
    }
    public JComboBox getKabupatenIHK(){
        return hk_cb_kabupatenI;
    }
    public JComboBox getKecamatanIHK(){
        return hk_cb_kecamatanI;
    }
    public JComboBox getKelurahanIHK(){
        return hk_cb_kelurahanI;
    }
    
    //Pendidikan
    public JComboBox getTingkatPK(){
        return pk_cb_tingkat;
    }
    public JTextField getNamaPK(){
        return pk_tf_nama_sekolah;
    }
    public JComboBox getProvinsiPK(){
        return pk_cb_provinsi;
    }
    public JComboBox getKabupatenPK(){
        return pk_cb_kabupaten;
    }
    public JTextField getTahunPK(){
        return pk_tf_tahun;
    }
    
    //Perkara Pidana
    public JRadioButton getPernahPP(){
        return pp_rb_pernah;
    }
    public JRadioButton getBelumPP(){
        return pp_rb_tidakpernah;
    }
    public JTextField getPerkaraPP(){
        return pp_tf_perkara;
    }
    public JTextField getPutusanPP(){
        return pp_tf_putusan;
    }
    public JTextField getKasusPP(){
        return pp_tf_kasus;
    }
    public JTextField getHukumanPP(){
        return pp_tf_hukuman;
    }
    public JRadioButton getPernahPPP(){
        return pp_rb_pernahP;
    }
    public JRadioButton getBelumPPP(){
        return pp_rb_tidakpernahP;
    }
    public JTextField getHukumPPP(){
        return pp_tf_hukumP;
    }
    public JTextField getProses(){
        return pp_tf_proses;
    }
    
    //Ciri Fisik
    public JTextField getRambutCF(){
        return cf_tf_rambut;
    }
    public JTextField getWajahCF(){
        return cf_tf_wajah;
    }
    public JTextField getKulitCF(){
        return cf_tf_kulit;
    }
    public JTextField getTInggiCF(){
        return cf_tf_tinggi;
    }
    public JTextField getBeratCF(){
        return cf_tf_berat;
    }
    public JTextField getTandaCF(){
        return cf_tf_tanda;
    }
    public JTextField getJariKiriCF(){
        return cf_tf_jariKiri;
    }
    public JTextField getJariKananCF(){
        return cf_tf_jariKanan;
    }
    
    //Keterangan
    public JTextField getRiwayatKT(){
        return kt_tf_riwayat;
    }
    public JTextField getHobiKT(){
        return kt_tf_hobi;
    }
    public JTextField getAlamatKT(){
        return kt_tf_alamat;
    }
    public JTextField getAlamatEmailKT(){
        return kt_tf_alamatEmail;
    }
    public JTextField getSponsorKT(){
        return kt_tf_sponsor;
    }
    public JTextField getAlamatSponsorKT(){
        return kt_tf_alamatSponsor;
    }
    public JTextField getTeleponKT(){
        return kt_tf_telepon;
    }
    public JTextField getJenisUsaha(){
        return kt_tf_jenisUsaha;
    }
    
    
     public HOME() {
        
        initComponents();
        
        sw=new satuan_wilayah_controller(this);
        dp=new data_pribadi_controller(this);
        hk=new hubungan_keluarga_controller(this);
        pk=new pendidikan_controller(this);
        //pp=new perkara_pidana_controller(this);
        //cf=new ciri_fisik_controller(this);
        //lp=new lampiran_controller(this);
        //kt=new keterangan_controller(this);
        
        
           
        tampil_dp_provinsi();
        tampil_dp_kabupaten();
        tampil_dp_kecamatan();
        tampil_dp_kelurahan();

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dp_rb_jenisKelamin = new javax.swing.ButtonGroup();
        dp_rb_perkawinan = new javax.swing.ButtonGroup();
        dp_rb_kewarganegaraan = new javax.swing.ButtonGroup();
        hk_rb_kewarganegaraan_1 = new javax.swing.ButtonGroup();
        hk_rb_kewarganegaraan_2 = new javax.swing.ButtonGroup();
        hk_rb_kewarganegaraan_3 = new javax.swing.ButtonGroup();
        pp_rb_perkara_pidana = new javax.swing.ButtonGroup();
        pp_rb_pelanggaran = new javax.swing.ButtonGroup();
        sw_rb_pembayaran = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jProgressBar8 = new javax.swing.JProgressBar();
        jLabel153 = new javax.swing.JLabel();
        sw_cb_keperluan = new javax.swing.JComboBox<>();
        jTextField50 = new javax.swing.JTextField();
        jLabel154 = new javax.swing.JLabel();
        sw_cb_kesatuan = new javax.swing.JComboBox<>();
        sw_cb_tingkat = new javax.swing.JComboBox<>();
        jTextField51 = new javax.swing.JTextField();
        jLabel155 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        sw_tf_alamat = new javax.swing.JTextArea();
        jLabel156 = new javax.swing.JLabel();
        sw_cb_provinsi = new javax.swing.JComboBox<>();
        jLabel157 = new javax.swing.JLabel();
        sw_cb_kabupaten = new javax.swing.JComboBox<>();
        jLabel158 = new javax.swing.JLabel();
        sw_cb_kecamatan = new javax.swing.JComboBox<>();
        jLabel159 = new javax.swing.JLabel();
        sw_cb_kelurahan = new javax.swing.JComboBox<>();
        jTextField52 = new javax.swing.JTextField();
        jLabel160 = new javax.swing.JLabel();
        sw_rb_tunai = new javax.swing.JRadioButton();
        sw_rb_briva = new javax.swing.JRadioButton();
        jLabel161 = new javax.swing.JLabel();
        sw_tf_namarek = new javax.swing.JTextField();
        sw_btn_batalkan = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        jLabel162 = new javax.swing.JLabel();
        jLabel163 = new javax.swing.JLabel();
        jLabel164 = new javax.swing.JLabel();
        jLabel165 = new javax.swing.JLabel();
        jLabel166 = new javax.swing.JLabel();
        jLabel167 = new javax.swing.JLabel();
        jLabel168 = new javax.swing.JLabel();
        jLabel169 = new javax.swing.JLabel();
        jLabel170 = new javax.swing.JLabel();
        jLabel171 = new javax.swing.JLabel();
        jLabel172 = new javax.swing.JLabel();
        jLabel173 = new javax.swing.JLabel();
        jLabel174 = new javax.swing.JLabel();
        jLabel175 = new javax.swing.JLabel();
        jLabel176 = new javax.swing.JLabel();
        jLabel177 = new javax.swing.JLabel();
        jLabel178 = new javax.swing.JLabel();
        jLabel179 = new javax.swing.JLabel();
        jLabel180 = new javax.swing.JLabel();
        jLabel181 = new javax.swing.JLabel();
        jLabel182 = new javax.swing.JLabel();
        jLabel183 = new javax.swing.JLabel();
        jLabel184 = new javax.swing.JLabel();
        jLabel185 = new javax.swing.JLabel();
        jLabel186 = new javax.swing.JLabel();
        jLabel187 = new javax.swing.JLabel();
        sw_btn_kembali = new javax.swing.JButton();
        sw_btn_lanjut = new javax.swing.JButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        jPanel20 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jLabel188 = new javax.swing.JLabel();
        dp_tf_pekerjaan = new javax.swing.JTextField();
        jLabel189 = new javax.swing.JLabel();
        dp_tf_notelp = new javax.swing.JTextField();
        jLabel190 = new javax.swing.JLabel();
        jLabel191 = new javax.swing.JLabel();
        dp_tf_nama = new javax.swing.JTextField();
        jLabel192 = new javax.swing.JLabel();
        dp_tf_tempat = new javax.swing.JTextField();
        jLabel193 = new javax.swing.JLabel();
        dp_tf_tanggal = new javax.swing.JTextField();
        jLabel194 = new javax.swing.JLabel();
        dp_rb_laki = new javax.swing.JRadioButton();
        dp_rb_perempuan = new javax.swing.JRadioButton();
        jLabel195 = new javax.swing.JLabel();
        dp_rb_kawin = new javax.swing.JRadioButton();
        dp_rb_belum = new javax.swing.JRadioButton();
        jLabel196 = new javax.swing.JLabel();
        dp_rb_wni = new javax.swing.JRadioButton();
        dp_rb_wna = new javax.swing.JRadioButton();
        jLabel197 = new javax.swing.JLabel();
        dp_cb_agama = new javax.swing.JComboBox<>();
        jScrollPane11 = new javax.swing.JScrollPane();
        dp_tf_alamat = new javax.swing.JTextArea();
        jLabel198 = new javax.swing.JLabel();
        dp_cb_provinsi = new javax.swing.JComboBox<>();
        jLabel199 = new javax.swing.JLabel();
        dp_cb_kabupaten = new javax.swing.JComboBox<>();
        jLabel200 = new javax.swing.JLabel();
        dp_cb_kecamatan = new javax.swing.JComboBox<>();
        jLabel201 = new javax.swing.JLabel();
        dp_cb_kelurahan = new javax.swing.JComboBox<>();
        jLabel202 = new javax.swing.JLabel();
        dp_cb_identitas = new javax.swing.JComboBox<>();
        jLabel203 = new javax.swing.JLabel();
        dp_tf_noIdentitas = new javax.swing.JTextField();
        dp_btn_batalkan = new javax.swing.JButton();
        dp_btn_ubah = new javax.swing.JButton();
        dp_btn_hapus = new javax.swing.JButton();
        jTextField61 = new javax.swing.JTextField();
        dp_btn_upload = new javax.swing.JButton();
        dp_btn_kembali = new javax.swing.JButton();
        dp_btn_lanjut = new javax.swing.JButton();
        jProgressBar9 = new javax.swing.JProgressBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jProgressBar10 = new javax.swing.JProgressBar();
        jLabel204 = new javax.swing.JLabel();
        jLabel205 = new javax.swing.JLabel();
        jLabel206 = new javax.swing.JLabel();
        hk_cb_hubungan = new javax.swing.JComboBox<>();
        jLabel207 = new javax.swing.JLabel();
        hk_tf_nama = new javax.swing.JTextField();
        jLabel208 = new javax.swing.JLabel();
        hk_tf_umur = new javax.swing.JTextField();
        jLabel209 = new javax.swing.JLabel();
        jLabel210 = new javax.swing.JLabel();
        hk_cb_agama = new javax.swing.JComboBox<>();
        jLabel211 = new javax.swing.JLabel();
        hk_rb_wni = new javax.swing.JRadioButton();
        hk_rb_wna = new javax.swing.JRadioButton();
        jLabel212 = new javax.swing.JLabel();
        hk_tf_pekerjaan = new javax.swing.JTextField();
        jLabel213 = new javax.swing.JLabel();
        jLabel214 = new javax.swing.JLabel();
        hk_cb_provinsi = new javax.swing.JComboBox<>();
        jLabel215 = new javax.swing.JLabel();
        hk_cb_kecamatan = new javax.swing.JComboBox<>();
        jLabel216 = new javax.swing.JLabel();
        hk_cb_kabupaten = new javax.swing.JComboBox<>();
        jLabel217 = new javax.swing.JLabel();
        hk_cb_kelurahan = new javax.swing.JComboBox<>();
        jLabel218 = new javax.swing.JLabel();
        jLabel219 = new javax.swing.JLabel();
        hk_cb_provinsiA = new javax.swing.JComboBox<>();
        jLabel220 = new javax.swing.JLabel();
        hk_cb_kecamatanA = new javax.swing.JComboBox<>();
        jLabel221 = new javax.swing.JLabel();
        hk_cb_kabupatenA = new javax.swing.JComboBox<>();
        jLabel222 = new javax.swing.JLabel();
        hk_cb_kelurahanA = new javax.swing.JComboBox<>();
        jLabel223 = new javax.swing.JLabel();
        jLabel224 = new javax.swing.JLabel();
        hk_tf_namaA = new javax.swing.JTextField();
        jLabel225 = new javax.swing.JLabel();
        hk_tf_umurA = new javax.swing.JTextField();
        jLabel226 = new javax.swing.JLabel();
        jLabel227 = new javax.swing.JLabel();
        hk_cb_agamaA = new javax.swing.JComboBox<>();
        jLabel228 = new javax.swing.JLabel();
        hk_rb_wniA = new javax.swing.JRadioButton();
        hk_rb_wnaA = new javax.swing.JRadioButton();
        jLabel229 = new javax.swing.JLabel();
        hk_tf_pekerjaanA = new javax.swing.JTextField();
        jScrollPane12 = new javax.swing.JScrollPane();
        hk_tf_alamatA = new javax.swing.JTextArea();
        jScrollPane13 = new javax.swing.JScrollPane();
        hk_tf_alamat = new javax.swing.JTextArea();
        jLabel230 = new javax.swing.JLabel();
        jLabel231 = new javax.swing.JLabel();
        hk_tf_namaI = new javax.swing.JTextField();
        jLabel232 = new javax.swing.JLabel();
        hk_tf_umurI = new javax.swing.JTextField();
        jLabel233 = new javax.swing.JLabel();
        jLabel234 = new javax.swing.JLabel();
        hk_cb_agamaI = new javax.swing.JComboBox<>();
        jLabel235 = new javax.swing.JLabel();
        hk_rb_wniI = new javax.swing.JRadioButton();
        hk_rb_wnaI = new javax.swing.JRadioButton();
        jLabel236 = new javax.swing.JLabel();
        hk_tf_pekerjaanI = new javax.swing.JTextField();
        jLabel237 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        hk_tf_alamatI = new javax.swing.JTextArea();
        jLabel238 = new javax.swing.JLabel();
        hk_cb_provinsiI = new javax.swing.JComboBox<>();
        jLabel239 = new javax.swing.JLabel();
        hk_cb_kecamatanI = new javax.swing.JComboBox<>();
        jLabel240 = new javax.swing.JLabel();
        hk_cb_kabupatenI = new javax.swing.JComboBox<>();
        jLabel241 = new javax.swing.JLabel();
        hk_cb_kelurahanI = new javax.swing.JComboBox<>();
        hk_btn_batal = new javax.swing.JButton();
        hk_btn_kembali = new javax.swing.JButton();
        hk_btn_lanjut = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jProgressBar11 = new javax.swing.JProgressBar();
        jLabel253 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jLabel254 = new javax.swing.JLabel();
        pk_cb_tingkat = new javax.swing.JComboBox<>();
        jLabel255 = new javax.swing.JLabel();
        pk_tf_nama_sekolah = new javax.swing.JTextField();
        jLabel256 = new javax.swing.JLabel();
        pk_cb_provinsi = new javax.swing.JComboBox<>();
        jLabel257 = new javax.swing.JLabel();
        pk_cb_kabupaten = new javax.swing.JComboBox<>();
        jLabel258 = new javax.swing.JLabel();
        pk_tf_tahun = new javax.swing.JTextField();
        pk_btn_hapus_riwayat = new javax.swing.JButton();
        pk_btn_tambah_riwayat = new javax.swing.JButton();
        pk_btn_batal = new javax.swing.JButton();
        pk_btn_kembali = new javax.swing.JButton();
        pk_btn_lanjut = new javax.swing.JButton();
        jScrollPane16 = new javax.swing.JScrollPane();
        jPanel25 = new javax.swing.JPanel();
        jProgressBar12 = new javax.swing.JProgressBar();
        jPanel26 = new javax.swing.JPanel();
        pp_rb_pernah = new javax.swing.JRadioButton();
        pp_rb_tidakpernah = new javax.swing.JRadioButton();
        jLabel259 = new javax.swing.JLabel();
        jLabel260 = new javax.swing.JLabel();
        pp_tf_perkara = new javax.swing.JTextField();
        jLabel261 = new javax.swing.JLabel();
        jLabel262 = new javax.swing.JLabel();
        pp_tf_putusan = new javax.swing.JTextField();
        jLabel263 = new javax.swing.JLabel();
        pp_tf_kasus = new javax.swing.JTextField();
        jLabel264 = new javax.swing.JLabel();
        pp_tf_hukuman = new javax.swing.JTextField();
        jLabel265 = new javax.swing.JLabel();
        pp_rb_pernahP = new javax.swing.JRadioButton();
        pp_rb_tidakpernahP = new javax.swing.JRadioButton();
        jLabel266 = new javax.swing.JLabel();
        pp_tf_hukumP = new javax.swing.JTextField();
        jLabel267 = new javax.swing.JLabel();
        jLabel268 = new javax.swing.JLabel();
        pp_tf_proses = new javax.swing.JTextField();
        pp_btn_batal = new javax.swing.JButton();
        pp_btn_kembali = new javax.swing.JButton();
        pp_btn_lanjut = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jProgressBar3 = new javax.swing.JProgressBar();
        jPanel28 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        cf_tf_rambut = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        cf_tf_wajah = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        cf_tf_kulit = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        cf_tf_tinggi = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        cf_tf_berat = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        cf_tf_tanda = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        cf_tf_jariKiri = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        cf_tf_jariKanan = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        cf_btn_batal = new javax.swing.JButton();
        cf_btn_kembali = new javax.swing.JButton();
        cf_btn_lanjut = new javax.swing.JButton();
        jScrollPane17 = new javax.swing.JScrollPane();
        jPanel29 = new javax.swing.JPanel();
        jPanel33 = new javax.swing.JPanel();
        jProgressBar17 = new javax.swing.JProgressBar();
        jPanel34 = new javax.swing.JPanel();
        jLabel313 = new javax.swing.JLabel();
        jLabel314 = new javax.swing.JLabel();
        LabelKtp = new javax.swing.JTextField();
        LabelPaspor = new javax.swing.JTextField();
        jLabel315 = new javax.swing.JLabel();
        LabelKk = new javax.swing.JTextField();
        jLabel316 = new javax.swing.JLabel();
        LabelAkte = new javax.swing.JTextField();
        jLabel317 = new javax.swing.JLabel();
        LabelSidik = new javax.swing.JTextField();
        jPanel35 = new javax.swing.JPanel();
        jLabel323 = new javax.swing.JLabel();
        But_Ktp = new javax.swing.JButton();
        But_paspor = new javax.swing.JButton();
        But_kk = new javax.swing.JButton();
        But_akte = new javax.swing.JButton();
        But_sidik = new javax.swing.JButton();
        lp_btn_batal = new javax.swing.JButton();
        lp_btn_kembali = new javax.swing.JButton();
        jScrollPane18 = new javax.swing.JScrollPane();
        jPanel36 = new javax.swing.JPanel();
        jPanel37 = new javax.swing.JPanel();
        kt_btn_batal = new javax.swing.JButton();
        jProgressBar4 = new javax.swing.JProgressBar();
        jPanel38 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        kt_tf_sponsor = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        kt_tf_alamatSponsor = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        kt_tf_telepon = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        kt_tf_jenisUsaha = new javax.swing.JTextField();
        jPanel39 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        kt_tf_riwayat = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        kt_tf_hobi = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        kt_tf_alamat = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        kt_tf_alamatEmail = new javax.swing.JTextField();
        kt_btn_proses = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jProgressBar8.setBackground(new java.awt.Color(0, 0, 0));
        jProgressBar8.setForeground(new java.awt.Color(51, 51, 255));
        jProgressBar8.setValue(12);

        jLabel153.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel153.setText("Silahkan pilih jenis keperluan pembuatan SKCK sesuai dengan tingkat kewenangan yang diperlukan");

        sw_cb_keperluan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Melamar Pekerjaan", "Melanjutkan Pendidikan" }));
        sw_cb_keperluan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sw_cb_keperluanActionPerformed(evt);
            }
        });

        jTextField50.setBackground(new java.awt.Color(255, 204, 204));
        jTextField50.setText("* Pilih Kesatuan Wilayah dibawah ini sesuai dengan domisili KTP atau Identitas daerah pemohon.");
        jTextField50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField50ActionPerformed(evt);
            }
        });

        jLabel154.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel154.setText("Silahkan pilih kesatuan wilayah untuk proses pembuatan dan pengambilan SKCK (Sesuai KTP)");

        sw_cb_kesatuan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mabes Polri", "Polda Bojong", "Polres Bojong", "Polsek Bojong" }));
        sw_cb_kesatuan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sw_cb_kesatuanActionPerformed(evt);
            }
        });

        sw_cb_tingkat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " Markas Besar Kepolisian Negara Republik Indonesia (Mabes Polri) ", " Kepolisian Negara Republik Indonesia Daerah (Polda) ", " Kepolisian Negara Republik Indonesia Resort Kota Besar (Polrestabes)", " Kepolisian Negara Republik Indonesia Resort Kota (Polresta)", " Kepolisian Negara Republik Indonesia Resort (Polres)", " Kepolisian Negara Republik Indonesia Sektor (Polsek) " }));
        sw_cb_tingkat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sw_cb_tingkatActionPerformed(evt);
            }
        });

        jTextField51.setBackground(new java.awt.Color(255, 204, 204));
        jTextField51.setText("* Isi data alamat dibawah ini sesuai dengan domisili KTP atau Identitas daerah pemohon.");

        jLabel155.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel155.setText("Alamat (Sesuai KTP)");

        sw_tf_alamat.setColumns(20);
        sw_tf_alamat.setRows(5);
        jScrollPane9.setViewportView(sw_tf_alamat);

        jLabel156.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel156.setText("Provinsi (Sesuai KTP)");

        sw_cb_provinsi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi" }));

        jLabel157.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel157.setText("Kabupaten / Kota (Sesuai KTP)");

        sw_cb_kabupaten.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten" }));

        jLabel158.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel158.setText("Kecamatan (Sesuai KTP)");

        sw_cb_kecamatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kecamatan" }));

        jLabel159.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel159.setText("Kelurahan (Sesuai KTP)");

        sw_cb_kelurahan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kelurahan" }));

        jTextField52.setBackground(new java.awt.Color(255, 204, 204));
        jTextField52.setText("Cara bayar yang akan dilakukan");

        jLabel160.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel160.setText("Cara Bayar ");

        sw_rb_pembayaran.add(sw_rb_tunai);
        sw_rb_tunai.setText("Tunai / loket");

        sw_rb_pembayaran.add(sw_rb_briva);
        sw_rb_briva.setText("BRIVA (BRI Virtual Account");

        jLabel161.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel161.setText("Atas Nama Rekening");

        sw_btn_batalkan.setText("Batalkan");

        jLabel162.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel162.setText("Informasi Keperluan Pembuatan SKCK dan Tingkat Kewenangan Kesatuan Wilayah");

        jLabel163.setText("A. MABES POLRI");

        jLabel164.setText("- Pencalonan Presiden dan Wakil Presiden");

        jLabel165.setText("- Pencalonan Anggota Legislatif, Eksekutif, Yudikatif, dan Lembaga Pemerintahan Tingkat Pusat");

        jLabel166.setText("- Penerbitan Visa");

        jLabel167.setText("- Ijin Tinggal Tetap di Luar Negeri");

        jLabel168.setText("- Naturalisasi Kewarganegaraan");

        jLabel169.setText("- Adopsi Anak Bagi Pemohon WNA");

        jLabel170.setText("- Melanjutkan Sekolah Luar Negeri");

        jLabel171.setText("B. POLDA ");

        jLabel172.setText("- Melamar Pekerjaan");

        jLabel173.setText("- Memperoleh Paspor atau Visa");

        jLabel174.setText("- Warga Negara Indonesia (WNI) Yang Akan Bekerja ke Luar Negeri");

        jLabel175.setText("- Menjadi Notaris");

        jLabel176.setText("- Pencalonan Pejabat Publik");

        jLabel177.setText("C. POLRES ");

        jLabel178.setText("- Pencalonan Anggota Legislatif Tingkat Kabupaten/Kota ");

        jLabel179.setText("- Melamar Sebagai PNS");

        jLabel180.setText("- Melamar Sebagai Anggota TNI/POLRI");

        jLabel181.setText("- Pencalonan Pejabat Publik");

        jLabel182.setText("- Pencalonan Kepala Daerah Tingkat Kabupaten/Kota");

        jLabel183.setText("D. POLSEK");

        jLabel184.setText("- Melamar Pekerjaan ");

        jLabel185.setText("- Pencalonan Kepala Desa");

        jLabel186.setText("- Pencalonan Sekertaris Desa ");

        jLabel187.setText("- Melanjutkan Sekolah");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel162)
                            .addComponent(jLabel163)
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel19Layout.createSequentialGroup()
                                        .addComponent(jLabel166, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(376, 376, 376))
                                    .addGroup(jPanel19Layout.createSequentialGroup()
                                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel165)
                                            .addComponent(jLabel164)
                                            .addComponent(jLabel167, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel168)
                                            .addComponent(jLabel170)
                                            .addComponent(jLabel169))
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(jLabel171)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel173)
                            .addComponent(jLabel172, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel174)
                            .addComponent(jLabel175)
                            .addComponent(jLabel176))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel177)
                            .addComponent(jLabel183)
                            .addGroup(jPanel19Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel179)
                                    .addComponent(jLabel178)
                                    .addComponent(jLabel180)
                                    .addComponent(jLabel181)
                                    .addComponent(jLabel182)
                                    .addComponent(jLabel184)
                                    .addComponent(jLabel185)
                                    .addComponent(jLabel186)
                                    .addComponent(jLabel187))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel162)
                .addGap(18, 18, 18)
                .addComponent(jLabel163)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel164)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel165)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel166)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel167)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel168)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel170)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel169)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel171)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel172)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel173)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel174)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel175)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel176)
                .addGap(18, 18, 18)
                .addComponent(jLabel177)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel178)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel179)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel180)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel181)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel182)
                .addGap(18, 18, 18)
                .addComponent(jLabel183)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel184)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel185)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel186)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel187)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        sw_btn_kembali.setText("Kembali");

        sw_btn_lanjut.setText("Lanjut");
        sw_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sw_btn_lanjutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel153, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(sw_tf_namarek, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(sw_cb_kecamatan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(sw_cb_kabupaten, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(sw_cb_provinsi, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                .addComponent(jTextField50, javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(sw_cb_keperluan, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jLabel154, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(sw_cb_kesatuan, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(sw_cb_tingkat, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jTextField51, javax.swing.GroupLayout.Alignment.LEADING))
                                                            .addComponent(jLabel155)))
                                                    .addComponent(jLabel156)))
                                            .addComponent(jLabel157)))
                                    .addComponent(jLabel158))
                                .addComponent(jLabel159)
                                .addComponent(sw_cb_kelurahan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField52))
                            .addComponent(jLabel160)
                            .addGroup(jPanel18Layout.createSequentialGroup()
                                .addComponent(sw_rb_tunai)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(sw_rb_briva))
                            .addComponent(jLabel161)))
                    .addComponent(sw_btn_batalkan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                        .addComponent(sw_btn_kembali)
                        .addGap(38, 38, 38)
                        .addComponent(sw_btn_lanjut)
                        .addGap(84, 84, 84))))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createSequentialGroup()
                        .addComponent(jLabel153)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_cb_keperluan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel154)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sw_cb_kesatuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sw_cb_tingkat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel155)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel156)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_cb_provinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel157)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_cb_kabupaten, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel158)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_cb_kecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel159)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_cb_kelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel160)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sw_rb_tunai)
                            .addComponent(sw_rb_briva))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel161)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sw_tf_namarek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sw_btn_batalkan)
                            .addComponent(sw_btn_kembali)
                            .addComponent(sw_btn_lanjut)))
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 40, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(804, Short.MAX_VALUE))
        );

        jScrollPane8.setViewportView(jPanel4);

        jTabbedPane1.addTab("Satuan Wilayah", jScrollPane8);

        jLabel188.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel188.setText("Pekerjaan");

        jLabel189.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel189.setText("No. Tlp / HP");

        jLabel190.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel190.setText("Alamat (Saat ini) ");

        jLabel191.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel191.setText("Nama Lengkap");

        dp_tf_nama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dp_tf_namaActionPerformed(evt);
            }
        });

        jLabel192.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel192.setText("Tempat Lahir");

        dp_tf_tempat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dp_tf_tempatActionPerformed(evt);
            }
        });

        jLabel193.setText("Tgl. Lahir");

        jLabel194.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel194.setText("Jenis Kelamin");

        dp_rb_jenisKelamin.add(dp_rb_laki);
        dp_rb_laki.setText("Laki - Laki");

        dp_rb_jenisKelamin.add(dp_rb_perempuan);
        dp_rb_perempuan.setText("Perempuan");

        jLabel195.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel195.setText("Status Perkawinan");

        dp_rb_perkawinan.add(dp_rb_kawin);
        dp_rb_kawin.setText("Kawin");

        dp_rb_perkawinan.add(dp_rb_belum);
        dp_rb_belum.setText("Belum Kawin");

        jLabel196.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel196.setText("Kewarganegaraan");

        dp_rb_kewarganegaraan.add(dp_rb_wni);
        dp_rb_wni.setText("WNI");

        dp_rb_kewarganegaraan.add(dp_rb_wna);
        dp_rb_wna.setText("WNA");

        jLabel197.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel197.setText("Agama");

        dp_cb_agama.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katholik", "Hindu", "Budha", "Konghucu" }));

        dp_tf_alamat.setColumns(20);
        dp_tf_alamat.setRows(5);
        jScrollPane11.setViewportView(dp_tf_alamat);

        jLabel198.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel198.setText("Provinsi (Saat ini)");

        dp_cb_provinsi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi", " " }));
        dp_cb_provinsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dp_cb_provinsiActionPerformed(evt);
            }
        });

        jLabel199.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel199.setText("Kabupaten / kota ( Saat ini )");

        dp_cb_kabupaten.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten / Kota" }));

        jLabel200.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel200.setText("Kecamatan ( Saat ini )");

        dp_cb_kecamatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kecamatan" }));

        jLabel201.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel201.setText("Keluruhan ( Saat ini ) ");

        dp_cb_kelurahan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Desa" }));

        jLabel202.setText("No. Identitas");

        dp_cb_identitas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "KTP", "SIM", "Paspor", "KITAS", " " }));

        jLabel203.setText("No. KTP/SIM/Paspor/KITAS");

        dp_btn_batalkan.setText("Batalkan");

        dp_btn_ubah.setText("UBAH");

        dp_btn_hapus.setText("HAPUS");

        dp_btn_upload.setText("UPLOAD");

        dp_btn_kembali.setText("kembali");

        dp_btn_lanjut.setText("lanjut");
        dp_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dp_btn_lanjutActionPerformed(evt);
            }
        });

        jProgressBar9.setValue(25);

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel191)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel192)
                        .addGap(296, 296, 296)
                        .addComponent(jLabel193))
                    .addComponent(jLabel190)
                    .addComponent(jLabel198)
                    .addComponent(jLabel199)
                    .addComponent(jLabel200)
                    .addComponent(jLabel201)
                    .addComponent(dp_btn_batalkan)
                    .addComponent(dp_tf_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(dp_tf_notelp, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel21Layout.createSequentialGroup()
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(dp_tf_tempat, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel21Layout.createSequentialGroup()
                                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel194)
                                        .addGroup(jPanel21Layout.createSequentialGroup()
                                            .addComponent(dp_rb_laki)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(dp_rb_perempuan))
                                        .addComponent(jLabel197))
                                    .addGap(36, 36, 36)
                                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel188)
                                        .addGroup(jPanel21Layout.createSequentialGroup()
                                            .addComponent(dp_rb_kawin)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(dp_rb_belum))
                                        .addComponent(jLabel195))))
                            .addGap(4, 4, 4)
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel21Layout.createSequentialGroup()
                                    .addComponent(dp_rb_wni)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(dp_rb_wna))
                                .addComponent(jLabel196)
                                .addGroup(jPanel21Layout.createSequentialGroup()
                                    .addGap(17, 17, 17)
                                    .addComponent(dp_tf_tanggal, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel21Layout.createSequentialGroup()
                                    .addGap(90, 90, 90)
                                    .addComponent(jLabel189))))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel21Layout.createSequentialGroup()
                            .addComponent(dp_cb_agama, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(36, 36, 36)
                            .addComponent(dp_tf_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(dp_cb_identitas, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dp_tf_noIdentitas, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(dp_cb_kelurahan, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dp_cb_kecamatan, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dp_cb_kabupaten, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dp_cb_provinsi, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel202)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel203)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                        .addComponent(jTextField61, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(160, 160, 160))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                        .addComponent(dp_btn_ubah)
                        .addGap(73, 73, 73)
                        .addComponent(dp_btn_hapus)
                        .addGap(143, 143, 143))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dp_btn_kembali)
                            .addComponent(dp_btn_upload))
                        .addGap(46, 46, 46)
                        .addComponent(dp_btn_lanjut)
                        .addGap(103, 103, 103))))
            .addComponent(jProgressBar9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jProgressBar9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel191)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(dp_tf_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel192)
                            .addComponent(jLabel193))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dp_tf_tempat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dp_tf_tanggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel194)
                            .addComponent(jLabel195)
                            .addComponent(jLabel196))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dp_rb_laki)
                            .addComponent(dp_rb_perempuan)
                            .addComponent(dp_rb_kawin)
                            .addComponent(dp_rb_belum)
                            .addComponent(dp_rb_wni)
                            .addComponent(dp_rb_wna))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel197)
                            .addComponent(jLabel188)
                            .addComponent(jLabel189)))
                    .addComponent(jTextField61, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dp_cb_agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dp_tf_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dp_tf_notelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel190)
                    .addComponent(dp_btn_ubah)
                    .addComponent(dp_btn_hapus))
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(dp_btn_upload)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel198)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dp_cb_provinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel199)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dp_cb_kabupaten, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel200)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dp_cb_kecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel201)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dp_cb_kelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel202)
                    .addComponent(jLabel203))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dp_cb_identitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dp_tf_noIdentitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dp_btn_kembali)
                    .addComponent(dp_btn_batalkan)
                    .addComponent(dp_btn_lanjut))
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 70, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(887, Short.MAX_VALUE))
        );

        jScrollPane10.setViewportView(jPanel20);

        jTabbedPane1.addTab("Data Pribadi", jScrollPane10);

        jProgressBar10.setValue(38);

        jLabel204.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel204.setText("Menerangkan hal-hal sebagai jawaban/keterangan atas pertanyaan-pertanyaan diajukan sebagai berikut:");

        jLabel205.setText("A. Data Istri / Suami");

        jLabel206.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel206.setText("Hubungan");

        hk_cb_hubungan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Orangtua", "Anak", "Saudara", "Kerabat" }));

        jLabel207.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel207.setText("Nama Lengkap");

        jLabel208.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel208.setText("Umur");

        jLabel209.setText("Tahun");

        jLabel210.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel210.setText("Agama ");

        hk_cb_agama.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katholik", "Hindu", "Budha", "Konghucu" }));

        jLabel211.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel211.setText("Kewarganegaraan");

        hk_rb_kewarganegaraan_1.add(hk_rb_wni);
        hk_rb_wni.setText("WNI");

        hk_rb_kewarganegaraan_1.add(hk_rb_wna);
        hk_rb_wna.setText("WNA");

        jLabel212.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel212.setText("Pekerjaan");

        jLabel213.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel213.setText("Alamat (Saat ini)");

        jLabel214.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel214.setText("Provinsi");

        hk_cb_provinsi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi" }));
        hk_cb_provinsi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hk_cb_provinsiActionPerformed(evt);
            }
        });

        jLabel215.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel215.setText("Kecamatan");

        hk_cb_kecamatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kecamatan" }));

        jLabel216.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel216.setText("Kabupaten / kota ");

        hk_cb_kabupaten.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten" }));

        jLabel217.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel217.setText("Kelurahan");

        hk_cb_kelurahan.setEditable(true);
        hk_cb_kelurahan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kelurahan" }));

        jLabel218.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel218.setText("Alamat (Saat ini)");

        jLabel219.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel219.setText("Provinsi");

        hk_cb_provinsiA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi" }));
        hk_cb_provinsiA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hk_cb_provinsiAActionPerformed(evt);
            }
        });

        jLabel220.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel220.setText("Kecamatan");

        hk_cb_kecamatanA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kecamtan" }));

        jLabel221.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel221.setText("Kabupaten / Kota");

        hk_cb_kabupatenA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten" }));

        jLabel222.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel222.setText("Kelurahan");

        hk_cb_kelurahanA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kelurahan" }));
        hk_cb_kelurahanA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hk_cb_kelurahanAActionPerformed(evt);
            }
        });

        jLabel223.setText("B. Data Ayah");

        jLabel224.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel224.setText("Nama Lengkap");

        jLabel225.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel225.setText("Umur");

        jLabel226.setText("Tahun");

        jLabel227.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel227.setText("Agama ");

        hk_cb_agamaA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katholik", "Hindu", "Budha", "Konghucu" }));

        jLabel228.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel228.setText("Kewarganegaraan");

        hk_rb_kewarganegaraan_2.add(hk_rb_wniA);
        hk_rb_wniA.setText("WNI");

        hk_rb_kewarganegaraan_2.add(hk_rb_wnaA);
        hk_rb_wnaA.setText("WNA");

        jLabel229.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel229.setText("Pekerjaan");

        hk_tf_alamatA.setColumns(20);
        hk_tf_alamatA.setRows(5);
        jScrollPane12.setViewportView(hk_tf_alamatA);

        hk_tf_alamat.setColumns(20);
        hk_tf_alamat.setRows(5);
        jScrollPane13.setViewportView(hk_tf_alamat);

        jLabel230.setText("C. Data Ibu");

        jLabel231.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel231.setText("Nama Lengkap");

        jLabel232.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel232.setText("Umur");

        jLabel233.setText("Tahun");

        jLabel234.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel234.setText("Agama ");

        hk_cb_agamaI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katholik", "Hindu", "Budha", "Konghucu" }));

        jLabel235.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel235.setText("Kewarganegaraan");

        hk_rb_kewarganegaraan_3.add(hk_rb_wniI);
        hk_rb_wniI.setText("WNI");

        hk_rb_kewarganegaraan_3.add(hk_rb_wnaI);
        hk_rb_wnaI.setText("WNA");

        jLabel236.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel236.setText("Pekerjaan");

        jLabel237.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel237.setText("Alamat (Saat ini)");

        hk_tf_alamatI.setColumns(20);
        hk_tf_alamatI.setRows(5);
        jScrollPane14.setViewportView(hk_tf_alamatI);

        jLabel238.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel238.setText("Provinsi");

        hk_cb_provinsiI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi" }));

        jLabel239.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel239.setText("Kecamatan");

        hk_cb_kecamatanI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kecamtan" }));

        jLabel240.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel240.setText("Kabupaten / Kota");

        hk_cb_kabupatenI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten" }));

        jLabel241.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel241.setText("Kelurahan");

        hk_cb_kelurahanI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kelurahan" }));

        hk_btn_batal.setText("Batalkan");

        hk_btn_kembali.setText("Kembali");

        hk_btn_lanjut.setText("Lanjut");
        hk_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hk_btn_lanjutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar10, javax.swing.GroupLayout.PREFERRED_SIZE, 1096, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel206)
                                    .addComponent(hk_cb_hubungan, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel210))
                                .addGap(48, 48, 48)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addGap(44, 44, 44)
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(jPanel10Layout.createSequentialGroup()
                                                    .addComponent(hk_rb_wni)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(hk_rb_wna))
                                                .addComponent(jLabel211))
                                            .addGap(51, 51, 51)
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel212)
                                                .addComponent(hk_tf_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addComponent(jLabel207)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel208)))
                                    .addGroup(jPanel10Layout.createSequentialGroup()
                                        .addComponent(hk_tf_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(60, 60, 60)
                                        .addComponent(hk_tf_umur, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel209))))
                            .addComponent(jLabel213)
                            .addComponent(hk_cb_agama, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel204, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel205)
                    .addComponent(jLabel223)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel214)
                            .addComponent(jLabel215)
                            .addComponent(hk_cb_provinsi, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hk_cb_kecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel221)
                            .addComponent(hk_cb_kabupatenA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel222)
                            .addComponent(hk_cb_kelurahanA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel216)
                                    .addComponent(hk_cb_kabupaten, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel217)
                                    .addComponent(hk_cb_kelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel227)
                                    .addComponent(hk_cb_agamaA, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel230)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                            .addGap(19, 19, 19)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel10Layout.createSequentialGroup()
                                    .addGap(2, 2, 2)
                                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel238)
                                        .addComponent(hk_cb_provinsiI, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel239)
                                        .addComponent(hk_cb_kecamatanI, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addGap(19, 19, 19)
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(hk_cb_kabupatenI, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel240)
                                                .addComponent(jLabel241)))
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(hk_cb_kelurahanI, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(jPanel10Layout.createSequentialGroup()
                                    .addComponent(jLabel235)
                                    .addGap(78, 78, 78)
                                    .addComponent(jLabel236))
                                .addGroup(jPanel10Layout.createSequentialGroup()
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel231)
                                                .addComponent(hk_tf_namaI, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(25, 25, 25)
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel232)
                                                .addGroup(jPanel10Layout.createSequentialGroup()
                                                    .addComponent(hk_tf_umurI, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel233))))
                                        .addGroup(jPanel10Layout.createSequentialGroup()
                                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(jLabel237)
                                                .addGroup(jPanel10Layout.createSequentialGroup()
                                                    .addComponent(hk_rb_wniI)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(hk_rb_wnaI)))
                                            .addGap(81, 81, 81)
                                            .addComponent(hk_tf_pekerjaanI, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(18, 18, 18)
                                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel234)
                                        .addComponent(hk_cb_agamaI, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(hk_rb_wniA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(hk_rb_wnaA))
                            .addComponent(jLabel228))
                        .addGap(68, 68, 68)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel229)
                            .addComponent(hk_tf_pekerjaanA, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel218)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel219)
                            .addComponent(hk_cb_provinsiA, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel220)
                            .addComponent(hk_cb_kecamatanA, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel224)
                            .addComponent(hk_tf_namaA, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(hk_tf_umurA, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel226))
                            .addComponent(jLabel225))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(hk_btn_batal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(hk_btn_kembali)
                .addGap(49, 49, 49)
                .addComponent(hk_btn_lanjut)
                .addGap(71, 71, 71))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel204)
                .addGap(18, 18, 18)
                .addComponent(jLabel205)
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel206)
                    .addComponent(jLabel207)
                    .addComponent(jLabel208))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_cb_hubungan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_tf_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_tf_umur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel209))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel210)
                    .addComponent(jLabel211)
                    .addComponent(jLabel212))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_cb_agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_rb_wni)
                    .addComponent(hk_rb_wna)
                    .addComponent(hk_tf_pekerjaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel213)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel214)
                            .addComponent(jLabel216))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hk_cb_provinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hk_cb_kabupaten, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel215)
                            .addComponent(jLabel217))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hk_cb_kecamatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hk_cb_kelurahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(jLabel223)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel224)
                    .addComponent(jLabel225)
                    .addComponent(jLabel227))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_tf_namaA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_tf_umurA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_cb_agamaA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel226))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel228)
                    .addComponent(jLabel229))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_rb_wniA)
                    .addComponent(hk_rb_wnaA)
                    .addComponent(hk_tf_pekerjaanA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel218)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addComponent(jLabel219)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hk_cb_provinsiA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel220)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(hk_cb_kecamatanA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel221)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(hk_cb_kabupatenA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel222)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hk_cb_kelurahanA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel230)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel231)
                    .addComponent(jLabel232)
                    .addComponent(jLabel234))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_tf_namaI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hk_tf_umurI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel233)
                    .addComponent(hk_cb_agamaI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel235)
                    .addComponent(jLabel236))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_rb_wniI)
                    .addComponent(hk_rb_wnaI)
                    .addComponent(hk_tf_pekerjaanI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel237)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel238)
                            .addComponent(jLabel240))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hk_cb_provinsiI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hk_cb_kabupatenI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel239)
                            .addComponent(jLabel241))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hk_cb_kecamatanI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hk_cb_kelurahanI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 131, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hk_btn_batal)
                    .addComponent(hk_btn_kembali)
                    .addComponent(hk_btn_lanjut))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 39, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 553, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel2);

        jTabbedPane1.addTab("Hubungan Keluarga", jScrollPane1);

        jProgressBar11.setValue(50);

        jLabel253.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        jLabel253.setText("Riwayat Pendidikan");

        jLabel254.setText("Pendidikan Terakhir");

        pk_cb_tingkat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SD", "SMP", "SMA", "SMK", "D1", "D2", "D3", "D4", "S1", "S2", "S3" }));

        jLabel255.setText("Nama Sekolah / Univ / Perguruan Tinggi");

        jLabel256.setText("Provinsi");

        pk_cb_provinsi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Provinsi" }));

        jLabel257.setText("Kab./ Kota");

        pk_cb_kabupaten.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Kabupaten" }));

        jLabel258.setText("Tahun");

        pk_btn_hapus_riwayat.setText("X Hapus Riwayat Pendidikan");

        pk_btn_tambah_riwayat.setText("+ Tambah Riwayat Pendidikan");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel254)
                    .addComponent(pk_cb_tingkat, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel255)
                            .addComponent(pk_tf_nama_sekolah, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel256)
                            .addComponent(pk_cb_provinsi, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel257)
                            .addComponent(pk_cb_kabupaten, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addComponent(pk_btn_hapus_riwayat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pk_btn_tambah_riwayat)))
                .addGap(39, 39, 39)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel258)
                    .addComponent(pk_tf_tahun, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(94, Short.MAX_VALUE))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel254)
                    .addComponent(jLabel255, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel256)
                    .addComponent(jLabel257)
                    .addComponent(jLabel258))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pk_cb_tingkat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pk_tf_nama_sekolah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pk_cb_provinsi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pk_cb_kabupaten, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pk_tf_tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pk_btn_hapus_riwayat)
                    .addComponent(pk_btn_tambah_riwayat))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pk_btn_batal.setText("Batalkan");

        pk_btn_kembali.setText("Kembali");

        pk_btn_lanjut.setText("Lanjut");
        pk_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pk_btn_lanjutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar11, javax.swing.GroupLayout.DEFAULT_SIZE, 1095, Short.MAX_VALUE)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel253)
                    .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(pk_btn_batal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pk_btn_kembali)
                .addGap(64, 64, 64)
                .addComponent(pk_btn_lanjut)
                .addGap(130, 130, 130))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar11, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel253)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pk_btn_batal)
                    .addComponent(pk_btn_kembali)
                    .addComponent(pk_btn_lanjut))
                .addContainerGap(80, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 40, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1197, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(jPanel3);

        jTabbedPane1.addTab("Pendidikan", jScrollPane2);

        jProgressBar12.setValue(63);

        pp_rb_perkara_pidana.add(pp_rb_pernah);
        pp_rb_pernah.setText("Pernah");

        pp_rb_perkara_pidana.add(pp_rb_tidakpernah);
        pp_rb_tidakpernah.setText("Tidak Pernah");

        jLabel259.setText("1. Perkara Pidana");

        jLabel260.setText("Apakah saudara pernah tersangkut perkara pidana ?");

        jLabel261.setText("Dalam perkara apa ?");

        jLabel262.setText("Bagaimana putusan vonis dan hakim ?");

        jLabel263.setText("Apakah saudara saat ini sedang dalam proses perkara pidana ? kasus apa ?");

        jLabel264.setText("Sampai sejauh mana proses hukumnya ?");

        jLabel265.setText("Apakah saudara pernah melakukan pelanggaran hukum dan atau norma norma sosial ? ");

        pp_rb_pelanggaran.add(pp_rb_pernahP);
        pp_rb_pernahP.setText("Pernah");

        pp_rb_pelanggaran.add(pp_rb_tidakpernahP);
        pp_rb_tidakpernahP.setText("Tidak Pernah");

        jLabel266.setText("Pelanggaran hukum atau norma norma sosial apa ?");

        jLabel267.setText("2. Pelanggaran");

        jLabel268.setText("Sampai sejauh mana prosesnya ?");

        pp_tf_proses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp_tf_prosesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel26Layout.createSequentialGroup()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel259))
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel260)
                            .addGroup(jPanel26Layout.createSequentialGroup()
                                .addComponent(pp_rb_pernah)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pp_rb_tidakpernah))
                            .addComponent(pp_tf_perkara, javax.swing.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
                            .addComponent(jLabel261)
                            .addComponent(jLabel262)
                            .addComponent(jLabel263)
                            .addComponent(pp_tf_kasus)
                            .addComponent(jLabel264, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pp_tf_hukuman)
                            .addComponent(pp_tf_putusan))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel267)
                    .addGroup(jPanel26Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel26Layout.createSequentialGroup()
                                .addComponent(pp_rb_pernahP)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pp_rb_tidakpernahP))
                            .addComponent(jLabel265)
                            .addComponent(jLabel266)
                            .addComponent(pp_tf_hukumP, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel268)
                            .addComponent(pp_tf_proses, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(82, 82, 82))
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel259)
                    .addComponent(jLabel267, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel260)
                    .addComponent(jLabel265))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pp_rb_pernah)
                    .addComponent(pp_rb_tidakpernah)
                    .addComponent(pp_rb_pernahP)
                    .addComponent(pp_rb_tidakpernahP))
                .addGap(3, 3, 3)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel261)
                    .addComponent(jLabel266, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pp_tf_perkara, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pp_tf_hukumP, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel262)
                    .addComponent(jLabel268))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pp_tf_putusan, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pp_tf_proses, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel263)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pp_tf_kasus, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel264)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pp_tf_hukuman, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(127, Short.MAX_VALUE))
        );

        pp_btn_batal.setText("BATALKAN");

        pp_btn_kembali.setText("KEMBALI");

        pp_btn_lanjut.setText("LANJUT");
        pp_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pp_btn_lanjutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar12, javax.swing.GroupLayout.DEFAULT_SIZE, 1135, Short.MAX_VALUE)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(pp_btn_batal, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pp_btn_kembali)
                .addGap(26, 26, 26)
                .addComponent(pp_btn_lanjut)
                .addGap(139, 139, 139))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pp_btn_batal)
                    .addComponent(pp_btn_kembali, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pp_btn_lanjut))
                .addContainerGap(1065, Short.MAX_VALUE))
        );

        jScrollPane16.setViewportView(jPanel25);

        jTabbedPane1.addTab("Perkara Pidana", jScrollPane16);

        jProgressBar3.setValue(75);

        jLabel26.setText("Rambut");

        jLabel27.setText("Wajah");

        jLabel28.setText("Kulit");

        jLabel29.setText("Tinggi Badan");

        jLabel30.setText("Cm");

        jLabel31.setText("Berat Badan");

        jLabel32.setText("Kg");

        jLabel33.setText("Tanda Istimewa");

        jLabel34.setText("Rumus sidik jari (kiri)");

        jLabel35.setText("Rumus sidik jari (kanan)");

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cf_tf_rambut)
                    .addComponent(cf_tf_wajah)
                    .addComponent(cf_tf_kulit)
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27)
                            .addComponent(jLabel28)
                            .addComponent(jLabel31)
                            .addComponent(jLabel33))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cf_tf_berat)
                            .addComponent(cf_tf_tanda, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cf_tf_tinggi, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel34)
                            .addComponent(cf_tf_jariKiri, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(79, 79, 79)
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel28Layout.createSequentialGroup()
                                .addComponent(jLabel35)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 107, Short.MAX_VALUE))
                            .addComponent(cf_tf_jariKanan))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cf_tf_rambut, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cf_tf_wajah, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cf_tf_kulit, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cf_tf_tinggi, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cf_tf_berat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel33)
                .addGap(1, 1, 1)
                .addComponent(cf_tf_tanda, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cf_tf_jariKiri, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cf_tf_jariKanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel36.setText("Ciri Fisik");

        cf_btn_batal.setText("BATALKAN");

        cf_btn_kembali.setText("KEMBALI");

        cf_btn_lanjut.setText("LANJUT");
        cf_btn_lanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cf_btn_lanjutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel17Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jProgressBar3, javax.swing.GroupLayout.PREFERRED_SIZE, 1094, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel36))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(cf_btn_batal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cf_btn_kembali)
                        .addGap(39, 39, 39)
                        .addComponent(cf_btn_lanjut)
                        .addGap(122, 122, 122))))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel36)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cf_btn_batal)
                    .addComponent(cf_btn_kembali)
                    .addComponent(cf_btn_lanjut))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 41, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1071, Short.MAX_VALUE))
        );

        jScrollPane6.setViewportView(jPanel5);

        jTabbedPane1.addTab("Ciri Fisik", jScrollPane6);

        jProgressBar17.setValue(88);

        jLabel313.setText("Ktp");

        jLabel314.setText("Paspor");

        jLabel315.setText("Kartu Keluarga");

        jLabel316.setText("Akte lahir / ijazah");

        jLabel317.setText("Sidik jari");

        jLabel323.setText("Unggah Lampiran");

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel323)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel323)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        But_Ktp.setText("Pilih lampiran");
        But_Ktp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                But_KtpActionPerformed(evt);
            }
        });

        But_paspor.setText("Pilih Lampiran");
        But_paspor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                But_pasporActionPerformed(evt);
            }
        });

        But_kk.setText("Pilh Lampiran");
        But_kk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                But_kkActionPerformed(evt);
            }
        });

        But_akte.setText("Plih lampiran");
        But_akte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                But_akteActionPerformed(evt);
            }
        });

        But_sidik.setText("Pilih Lampiran");
        But_sidik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                But_sidikActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel313)
                            .addComponent(jLabel314)
                            .addComponent(jLabel315)
                            .addComponent(jLabel316)
                            .addComponent(LabelPaspor, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                            .addComponent(LabelSidik)
                            .addComponent(LabelAkte)
                            .addComponent(LabelKk)
                            .addComponent(LabelKtp))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(But_Ktp)
                            .addComponent(But_paspor)
                            .addComponent(But_kk)
                            .addComponent(But_akte)
                            .addComponent(But_sidik)))
                    .addComponent(jLabel317))
                .addContainerGap(526, Short.MAX_VALUE))
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel313)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelKtp, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(But_Ktp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel314)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelPaspor, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(But_paspor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel315)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelKk, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(But_kk))
                .addGap(13, 13, 13)
                .addComponent(jLabel316, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelAkte, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(But_akte))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel317)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelSidik, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(But_sidik))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lp_btn_batal.setText("BATALKAN");

        lp_btn_kembali.setText("KEMBALI");

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar17, javax.swing.GroupLayout.DEFAULT_SIZE, 1119, Short.MAX_VALUE)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel33Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lp_btn_batal, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(lp_btn_kembali)
                .addGap(127, 127, 127))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar17, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lp_btn_batal)
                    .addComponent(lp_btn_kembali))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1105, Short.MAX_VALUE))
        );

        jScrollPane17.setViewportView(jPanel29);

        jTabbedPane1.addTab("Lampiran", jScrollPane17);

        kt_btn_batal.setText("BATALKAN");

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kt_btn_batal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(1032, Short.MAX_VALUE))
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(kt_btn_batal)
                .addContainerGap(458, Short.MAX_VALUE))
        );

        jProgressBar4.setValue(100);

        jLabel37.setText("Unggah Lampiran");

        jLabel38.setText("Sponsor (khusus orang asing)");

        jLabel39.setText("Disponsori oleh");

        jLabel40.setText("Alamat sponsor");

        jLabel41.setText("Telp./ fax");

        jLabel42.setText("Jenis usaha");

        jLabel43.setText("Riwayat pekerjaan / negara yang pernah dikunjungi (sebutkan tahun berapa, dalam rangka apa,");

        jLabel44.setText("dan negara mana yang dikunjungi ?");

        jLabel45.setText("Kesenangan / Kegemaran / Hobi");

        jLabel46.setText("Alamat yang mudah dihubungi dan no. telepon");

        jLabel47.setText("Alamat email");

        javax.swing.GroupLayout jPanel39Layout = new javax.swing.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel43)
            .addComponent(jLabel44)
            .addComponent(jLabel45)
            .addComponent(jLabel46)
            .addGroup(jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                .addComponent(kt_tf_hobi, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
                .addComponent(kt_tf_riwayat, javax.swing.GroupLayout.Alignment.LEADING))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel39Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel47)
                        .addComponent(kt_tf_alamat, javax.swing.GroupLayout.PREFERRED_SIZE, 456, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(kt_tf_alamatEmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 456, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel39Layout.createSequentialGroup()
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel44)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kt_tf_riwayat, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel45)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kt_tf_hobi, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel46)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kt_tf_alamat)
                .addGap(18, 18, 18)
                .addComponent(jLabel47)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(kt_tf_alamatEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
        );

        javax.swing.GroupLayout jPanel38Layout = new javax.swing.GroupLayout(jPanel38);
        jPanel38.setLayout(jPanel38Layout);
        jPanel38Layout.setHorizontalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addGap(62, 62, 62)
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel40)
                            .addComponent(jLabel38)
                            .addComponent(jLabel39))
                        .addContainerGap())
                    .addComponent(kt_tf_alamatSponsor)
                    .addComponent(kt_tf_telepon)
                    .addComponent(kt_tf_sponsor)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel41)
                            .addComponent(jLabel42)
                            .addComponent(kt_tf_jenisUsaha, javax.swing.GroupLayout.PREFERRED_SIZE, 444, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel38Layout.setVerticalGroup(
            jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel38Layout.createSequentialGroup()
                .addGroup(jPanel38Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel38)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kt_tf_sponsor, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kt_tf_alamatSponsor, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kt_tf_telepon, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kt_tf_jenisUsaha, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel38Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        kt_btn_proses.setText("Proses");
        kt_btn_proses.setActionCommand("PROSES");
        kt_btn_proses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kt_btn_prosesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar4, javax.swing.GroupLayout.DEFAULT_SIZE, 1135, Short.MAX_VALUE)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, 908, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel36Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(kt_btn_proses)
                        .addGap(9, 9, 9)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel36Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(138, 138, 138)
                .addComponent(kt_btn_proses)
                .addContainerGap(1008, Short.MAX_VALUE))
            .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel36Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jScrollPane18.setViewportView(jPanel36);

        jTabbedPane1.addTab("Keterangan", jScrollPane18);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jLabel1.setText("Form. Pendaftaran");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1346, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField50ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField50ActionPerformed

    private void sw_cb_kesatuanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sw_cb_kesatuanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sw_cb_kesatuanActionPerformed

    private void sw_cb_tingkatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sw_cb_tingkatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sw_cb_tingkatActionPerformed

    private void dp_cb_provinsiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dp_cb_provinsiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dp_cb_provinsiActionPerformed

    private void hk_cb_kelurahanAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hk_cb_kelurahanAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hk_cb_kelurahanAActionPerformed

    private void hk_cb_provinsiAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hk_cb_provinsiAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hk_cb_provinsiAActionPerformed

    private void hk_cb_provinsiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hk_cb_provinsiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_hk_cb_provinsiActionPerformed

    private void dp_tf_namaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dp_tf_namaActionPerformed
        
    }//GEN-LAST:event_dp_tf_namaActionPerformed

    private void dp_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dp_btn_lanjutActionPerformed
           dp.simpanForm();
    }//GEN-LAST:event_dp_btn_lanjutActionPerformed

    private void dp_tf_tempatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dp_tf_tempatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dp_tf_tempatActionPerformed

    private void hk_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hk_btn_lanjutActionPerformed
        hk.simpanForm();
    }//GEN-LAST:event_hk_btn_lanjutActionPerformed

    private void sw_cb_keperluanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sw_cb_keperluanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sw_cb_keperluanActionPerformed

    private void sw_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sw_btn_lanjutActionPerformed
        sw.simpanForm();
        jtp.setSelectedIndex(2);
    }//GEN-LAST:event_sw_btn_lanjutActionPerformed

    private void pk_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pk_btn_lanjutActionPerformed
        pk.simpanForm();
    }//GEN-LAST:event_pk_btn_lanjutActionPerformed

    private void pp_tf_prosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp_tf_prosesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pp_tf_prosesActionPerformed

    private void pp_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pp_btn_lanjutActionPerformed
        pp.simpanForm();        // TODO add your handling code here:
    }//GEN-LAST:event_pp_btn_lanjutActionPerformed

    private void cf_btn_lanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cf_btn_lanjutActionPerformed
        cf.simpanForm();        // TODO add your handling code here:
    }//GEN-LAST:event_cf_btn_lanjutActionPerformed

    private void kt_btn_prosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kt_btn_prosesActionPerformed
        kt.simpanForm();        // TODO add your handling code here:
    }//GEN-LAST:event_kt_btn_prosesActionPerformed

    private void But_KtpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_But_KtpActionPerformed
        // TODO add your handling code here:
        int vktp=ktp.showOpenDialog(this);
        
        if(vktp==ktp.APPROVE_OPTION){
            File pktp=ktp.getSelectedFile();
            LabelKtp.setText(pktp.getPath());
        }
    }//GEN-LAST:event_But_KtpActionPerformed

    private void But_pasporActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_But_pasporActionPerformed
        // TODO add your handling code here:
        int vpaspor=paspor.showOpenDialog(this);
        
        if(vpaspor==paspor.APPROVE_OPTION){
            File ppaspor=paspor.getSelectedFile();
            LabelPaspor.setText(ppaspor.getPath());
        }
    }//GEN-LAST:event_But_pasporActionPerformed

    private void But_kkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_But_kkActionPerformed
        // TODO add your handling code here:
        int vkk=kk.showOpenDialog(this);
        
        if(vkk==kk.APPROVE_OPTION){
            File pkk=kk.getSelectedFile();
            LabelKk.setText(pkk.getPath());
        }
    }//GEN-LAST:event_But_kkActionPerformed

    private void But_akteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_But_akteActionPerformed
        // TODO add your handling code here:
        int vakte=akte.showOpenDialog(this);
        
        if(vakte==akte.APPROVE_OPTION){
            File pakte=akte.getSelectedFile();
            LabelAkte.setText(pakte.getPath());
        }
    }//GEN-LAST:event_But_akteActionPerformed

    private void But_sidikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_But_sidikActionPerformed
        // TODO add your handling code here:
        int vsidik=sidik.showOpenDialog(this);
        
        if(vsidik==sidik.APPROVE_OPTION){
            File psidik=sidik.getSelectedFile();
            LabelSidik.setText(psidik.getPath());
        }
    }//GEN-LAST:event_But_sidikActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HOME().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton But_Ktp;
    private javax.swing.JButton But_akte;
    private javax.swing.JButton But_kk;
    private javax.swing.JButton But_paspor;
    private javax.swing.JButton But_sidik;
    private javax.swing.JTextField LabelAkte;
    private javax.swing.JTextField LabelKk;
    private javax.swing.JTextField LabelKtp;
    private javax.swing.JTextField LabelPaspor;
    private javax.swing.JTextField LabelSidik;
    private javax.swing.JButton cf_btn_batal;
    private javax.swing.JButton cf_btn_kembali;
    private javax.swing.JButton cf_btn_lanjut;
    private javax.swing.JTextField cf_tf_berat;
    private javax.swing.JTextField cf_tf_jariKanan;
    private javax.swing.JTextField cf_tf_jariKiri;
    private javax.swing.JTextField cf_tf_kulit;
    private javax.swing.JTextField cf_tf_rambut;
    private javax.swing.JTextField cf_tf_tanda;
    private javax.swing.JTextField cf_tf_tinggi;
    private javax.swing.JTextField cf_tf_wajah;
    private javax.swing.JButton dp_btn_batalkan;
    private javax.swing.JButton dp_btn_hapus;
    private javax.swing.JButton dp_btn_kembali;
    public javax.swing.JButton dp_btn_lanjut;
    private javax.swing.JButton dp_btn_ubah;
    private javax.swing.JButton dp_btn_upload;
    private javax.swing.JComboBox<String> dp_cb_agama;
    private javax.swing.JComboBox<String> dp_cb_identitas;
    public javax.swing.JComboBox<String> dp_cb_kabupaten;
    public javax.swing.JComboBox<String> dp_cb_kecamatan;
    public javax.swing.JComboBox<String> dp_cb_kelurahan;
    public javax.swing.JComboBox<String> dp_cb_provinsi;
    private javax.swing.JRadioButton dp_rb_belum;
    private javax.swing.ButtonGroup dp_rb_jenisKelamin;
    private javax.swing.JRadioButton dp_rb_kawin;
    private javax.swing.ButtonGroup dp_rb_kewarganegaraan;
    private javax.swing.JRadioButton dp_rb_laki;
    private javax.swing.JRadioButton dp_rb_perempuan;
    private javax.swing.ButtonGroup dp_rb_perkawinan;
    private javax.swing.JRadioButton dp_rb_wna;
    private javax.swing.JRadioButton dp_rb_wni;
    private javax.swing.JTextArea dp_tf_alamat;
    private javax.swing.JTextField dp_tf_nama;
    private javax.swing.JTextField dp_tf_noIdentitas;
    private javax.swing.JTextField dp_tf_notelp;
    private javax.swing.JTextField dp_tf_pekerjaan;
    private javax.swing.JTextField dp_tf_tanggal;
    private javax.swing.JTextField dp_tf_tempat;
    private javax.swing.JButton hk_btn_batal;
    private javax.swing.JButton hk_btn_kembali;
    private javax.swing.JButton hk_btn_lanjut;
    private javax.swing.JComboBox<String> hk_cb_agama;
    private javax.swing.JComboBox<String> hk_cb_agamaA;
    private javax.swing.JComboBox<String> hk_cb_agamaI;
    private javax.swing.JComboBox<String> hk_cb_hubungan;
    private javax.swing.JComboBox<String> hk_cb_kabupaten;
    private javax.swing.JComboBox<String> hk_cb_kabupatenA;
    private javax.swing.JComboBox<String> hk_cb_kabupatenI;
    private javax.swing.JComboBox<String> hk_cb_kecamatan;
    private javax.swing.JComboBox<String> hk_cb_kecamatanA;
    private javax.swing.JComboBox<String> hk_cb_kecamatanI;
    private javax.swing.JComboBox<String> hk_cb_kelurahan;
    private javax.swing.JComboBox<String> hk_cb_kelurahanA;
    private javax.swing.JComboBox<String> hk_cb_kelurahanI;
    private javax.swing.JComboBox<String> hk_cb_provinsi;
    private javax.swing.JComboBox<String> hk_cb_provinsiA;
    private javax.swing.JComboBox<String> hk_cb_provinsiI;
    private javax.swing.ButtonGroup hk_rb_kewarganegaraan_1;
    private javax.swing.ButtonGroup hk_rb_kewarganegaraan_2;
    private javax.swing.ButtonGroup hk_rb_kewarganegaraan_3;
    private javax.swing.JRadioButton hk_rb_wna;
    private javax.swing.JRadioButton hk_rb_wnaA;
    private javax.swing.JRadioButton hk_rb_wnaI;
    private javax.swing.JRadioButton hk_rb_wni;
    private javax.swing.JRadioButton hk_rb_wniA;
    private javax.swing.JRadioButton hk_rb_wniI;
    private javax.swing.JTextArea hk_tf_alamat;
    private javax.swing.JTextArea hk_tf_alamatA;
    private javax.swing.JTextArea hk_tf_alamatI;
    private javax.swing.JTextField hk_tf_nama;
    private javax.swing.JTextField hk_tf_namaA;
    private javax.swing.JTextField hk_tf_namaI;
    private javax.swing.JTextField hk_tf_pekerjaan;
    private javax.swing.JTextField hk_tf_pekerjaanA;
    private javax.swing.JTextField hk_tf_pekerjaanI;
    private javax.swing.JTextField hk_tf_umur;
    private javax.swing.JTextField hk_tf_umurA;
    private javax.swing.JTextField hk_tf_umurI;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel153;
    private javax.swing.JLabel jLabel154;
    private javax.swing.JLabel jLabel155;
    private javax.swing.JLabel jLabel156;
    private javax.swing.JLabel jLabel157;
    private javax.swing.JLabel jLabel158;
    private javax.swing.JLabel jLabel159;
    private javax.swing.JLabel jLabel160;
    private javax.swing.JLabel jLabel161;
    private javax.swing.JLabel jLabel162;
    private javax.swing.JLabel jLabel163;
    private javax.swing.JLabel jLabel164;
    private javax.swing.JLabel jLabel165;
    private javax.swing.JLabel jLabel166;
    private javax.swing.JLabel jLabel167;
    private javax.swing.JLabel jLabel168;
    private javax.swing.JLabel jLabel169;
    private javax.swing.JLabel jLabel170;
    private javax.swing.JLabel jLabel171;
    private javax.swing.JLabel jLabel172;
    private javax.swing.JLabel jLabel173;
    private javax.swing.JLabel jLabel174;
    private javax.swing.JLabel jLabel175;
    private javax.swing.JLabel jLabel176;
    private javax.swing.JLabel jLabel177;
    private javax.swing.JLabel jLabel178;
    private javax.swing.JLabel jLabel179;
    private javax.swing.JLabel jLabel180;
    private javax.swing.JLabel jLabel181;
    private javax.swing.JLabel jLabel182;
    private javax.swing.JLabel jLabel183;
    private javax.swing.JLabel jLabel184;
    private javax.swing.JLabel jLabel185;
    private javax.swing.JLabel jLabel186;
    private javax.swing.JLabel jLabel187;
    private javax.swing.JLabel jLabel188;
    private javax.swing.JLabel jLabel189;
    private javax.swing.JLabel jLabel190;
    private javax.swing.JLabel jLabel191;
    private javax.swing.JLabel jLabel192;
    private javax.swing.JLabel jLabel193;
    private javax.swing.JLabel jLabel194;
    private javax.swing.JLabel jLabel195;
    private javax.swing.JLabel jLabel196;
    private javax.swing.JLabel jLabel197;
    private javax.swing.JLabel jLabel198;
    private javax.swing.JLabel jLabel199;
    private javax.swing.JLabel jLabel200;
    private javax.swing.JLabel jLabel201;
    private javax.swing.JLabel jLabel202;
    private javax.swing.JLabel jLabel203;
    private javax.swing.JLabel jLabel204;
    private javax.swing.JLabel jLabel205;
    private javax.swing.JLabel jLabel206;
    private javax.swing.JLabel jLabel207;
    private javax.swing.JLabel jLabel208;
    private javax.swing.JLabel jLabel209;
    private javax.swing.JLabel jLabel210;
    private javax.swing.JLabel jLabel211;
    private javax.swing.JLabel jLabel212;
    private javax.swing.JLabel jLabel213;
    private javax.swing.JLabel jLabel214;
    private javax.swing.JLabel jLabel215;
    private javax.swing.JLabel jLabel216;
    private javax.swing.JLabel jLabel217;
    private javax.swing.JLabel jLabel218;
    private javax.swing.JLabel jLabel219;
    private javax.swing.JLabel jLabel220;
    private javax.swing.JLabel jLabel221;
    private javax.swing.JLabel jLabel222;
    private javax.swing.JLabel jLabel223;
    private javax.swing.JLabel jLabel224;
    private javax.swing.JLabel jLabel225;
    private javax.swing.JLabel jLabel226;
    private javax.swing.JLabel jLabel227;
    private javax.swing.JLabel jLabel228;
    private javax.swing.JLabel jLabel229;
    private javax.swing.JLabel jLabel230;
    private javax.swing.JLabel jLabel231;
    private javax.swing.JLabel jLabel232;
    private javax.swing.JLabel jLabel233;
    private javax.swing.JLabel jLabel234;
    private javax.swing.JLabel jLabel235;
    private javax.swing.JLabel jLabel236;
    private javax.swing.JLabel jLabel237;
    private javax.swing.JLabel jLabel238;
    private javax.swing.JLabel jLabel239;
    private javax.swing.JLabel jLabel240;
    private javax.swing.JLabel jLabel241;
    private javax.swing.JLabel jLabel253;
    private javax.swing.JLabel jLabel254;
    private javax.swing.JLabel jLabel255;
    private javax.swing.JLabel jLabel256;
    private javax.swing.JLabel jLabel257;
    private javax.swing.JLabel jLabel258;
    private javax.swing.JLabel jLabel259;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel260;
    private javax.swing.JLabel jLabel261;
    private javax.swing.JLabel jLabel262;
    private javax.swing.JLabel jLabel263;
    private javax.swing.JLabel jLabel264;
    private javax.swing.JLabel jLabel265;
    private javax.swing.JLabel jLabel266;
    private javax.swing.JLabel jLabel267;
    private javax.swing.JLabel jLabel268;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel313;
    private javax.swing.JLabel jLabel314;
    private javax.swing.JLabel jLabel315;
    private javax.swing.JLabel jLabel316;
    private javax.swing.JLabel jLabel317;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel323;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JProgressBar jProgressBar10;
    private javax.swing.JProgressBar jProgressBar11;
    private javax.swing.JProgressBar jProgressBar12;
    private javax.swing.JProgressBar jProgressBar17;
    private javax.swing.JProgressBar jProgressBar3;
    private javax.swing.JProgressBar jProgressBar4;
    private javax.swing.JProgressBar jProgressBar8;
    private javax.swing.JProgressBar jProgressBar9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    public javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField50;
    private javax.swing.JTextField jTextField51;
    private javax.swing.JTextField jTextField52;
    private javax.swing.JTextField jTextField61;
    private javax.swing.JButton kt_btn_batal;
    private javax.swing.JToggleButton kt_btn_proses;
    private javax.swing.JTextField kt_tf_alamat;
    private javax.swing.JTextField kt_tf_alamatEmail;
    private javax.swing.JTextField kt_tf_alamatSponsor;
    private javax.swing.JTextField kt_tf_hobi;
    private javax.swing.JTextField kt_tf_jenisUsaha;
    private javax.swing.JTextField kt_tf_riwayat;
    private javax.swing.JTextField kt_tf_sponsor;
    private javax.swing.JTextField kt_tf_telepon;
    private javax.swing.JButton lp_btn_batal;
    private javax.swing.JButton lp_btn_kembali;
    private javax.swing.JButton pk_btn_batal;
    private javax.swing.JButton pk_btn_hapus_riwayat;
    private javax.swing.JButton pk_btn_kembali;
    private javax.swing.JButton pk_btn_lanjut;
    private javax.swing.JButton pk_btn_tambah_riwayat;
    private javax.swing.JComboBox<String> pk_cb_kabupaten;
    private javax.swing.JComboBox<String> pk_cb_provinsi;
    private javax.swing.JComboBox<String> pk_cb_tingkat;
    private javax.swing.JTextField pk_tf_nama_sekolah;
    private javax.swing.JTextField pk_tf_tahun;
    private javax.swing.JButton pp_btn_batal;
    private javax.swing.JButton pp_btn_kembali;
    private javax.swing.JButton pp_btn_lanjut;
    private javax.swing.ButtonGroup pp_rb_pelanggaran;
    private javax.swing.ButtonGroup pp_rb_perkara_pidana;
    private javax.swing.JRadioButton pp_rb_pernah;
    private javax.swing.JRadioButton pp_rb_pernahP;
    private javax.swing.JRadioButton pp_rb_tidakpernah;
    private javax.swing.JRadioButton pp_rb_tidakpernahP;
    private javax.swing.JTextField pp_tf_hukumP;
    private javax.swing.JTextField pp_tf_hukuman;
    private javax.swing.JTextField pp_tf_kasus;
    private javax.swing.JTextField pp_tf_perkara;
    private javax.swing.JTextField pp_tf_proses;
    private javax.swing.JTextField pp_tf_putusan;
    private javax.swing.JButton sw_btn_batalkan;
    private javax.swing.JButton sw_btn_kembali;
    private javax.swing.JButton sw_btn_lanjut;
    private javax.swing.JComboBox<String> sw_cb_kabupaten;
    private javax.swing.JComboBox<String> sw_cb_kecamatan;
    private javax.swing.JComboBox<String> sw_cb_kelurahan;
    private javax.swing.JComboBox<String> sw_cb_keperluan;
    private javax.swing.JComboBox<String> sw_cb_kesatuan;
    private javax.swing.JComboBox<String> sw_cb_provinsi;
    private javax.swing.JComboBox<String> sw_cb_tingkat;
    private javax.swing.JRadioButton sw_rb_briva;
    private javax.swing.ButtonGroup sw_rb_pembayaran;
    private javax.swing.JRadioButton sw_rb_tunai;
    private javax.swing.JTextArea sw_tf_alamat;
    private javax.swing.JTextField sw_tf_namarek;
    // End of variables declaration//GEN-END:variables

    private void simpanForm(String nama) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
