/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

/**
 *
 * @author acer
 */
public class perkara_pidana_model {
    
    String perkara,hukuman,tersangkut,kasus,lamaproses;

    public String getPerkara() {
        return perkara;
    }

    public void setPerkara(String perkara) {
        this.perkara = perkara;
    }

    public String getHukuman() {
        return hukuman;
    }

    public void setHukuman(String hukuman) {
        this.hukuman = hukuman;
    }

    public String getTersangkut() {
        return tersangkut;
    }

    public void setTersangkut(String tersangkut) {
        this.tersangkut = tersangkut;
    }

    public String getKasus() {
        return kasus;
    }

    public void setKasus(String kasus) {
        this.kasus = kasus;
    }

    public String getLamaproses() {
        return lamaproses;
    }

    public void setLamaproses(String lamaproses) {
        this.lamaproses = lamaproses;
    }
    
    
}
