/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

/**
 *
 * @author acer
 */
public class hubungan_keluarga_model {
    
    String hubunganp,agamap,alamatp,namap,kwnp,pekerjanp,provinsip,kecamatanp,umurp,kabupatenp,kelurahanp,namaa,umura,agamaa,kwna,pekerjaana,alamata,provinsia,kecamatana,kabupatena,kelurahana,namai,umuri,agamai,kwni,pekerjaani,alamati,provinsii,kecamatani,kabupateni,kelurahani;

    public String getHubunganp() {
        return hubunganp;
    }

    public void setHubunganp(String hubunganp) {
        this.hubunganp = hubunganp;
    }

    public String getAgamap() {
        return agamap;
    }

    public void setAgamap(String agamap) {
        this.agamap = agamap;
    }

    public String getAlamatp() {
        return alamatp;
    }

    public void setAlamatp(String alamatp) {
        this.alamatp = alamatp;
    }

    public String getNamap() {
        return namap;
    }

    public void setNamap(String namap) {
        this.namap = namap;
    }

    public String getKwnp() {
        return kwnp;
    }

    public void setKwnp(String kwnp) {
        this.kwnp = kwnp;
    }

    public String getPekerjanp() {
        return pekerjanp;
    }

    public void setPekerjanp(String pekerjanp) {
        this.pekerjanp = pekerjanp;
    }

    public String getProvinsip() {
        return provinsip;
    }

    public void setProvinsip(String provinsip) {
        this.provinsip = provinsip;
    }

    public String getKecamatanp() {
        return kecamatanp;
    }

    public void setKecamatanp(String kecamatanp) {
        this.kecamatanp = kecamatanp;
    }

    public String getUmurp() {
        return umurp;
    }

    public void setUmurp(String umurp) {
        this.umurp = umurp;
    }

    public String getKabupatenp() {
        return kabupatenp;
    }

    public void setKabupatenp(String kabupatenp) {
        this.kabupatenp = kabupatenp;
    }

    public String getKelurahanp() {
        return kelurahanp;
    }

    public void setKelurahanp(String kelurahanp) {
        this.kelurahanp = kelurahanp;
    }

    public String getNamaa() {
        return namaa;
    }

    public void setNamaa(String namaa) {
        this.namaa = namaa;
    }

    public String getUmura() {
        return umura;
    }

    public void setUmura(String umura) {
        this.umura = umura;
    }

    public String getAgamaa() {
        return agamaa;
    }

    public void setAgamaa(String agamaa) {
        this.agamaa = agamaa;
    }

    public String getKwna() {
        return kwna;
    }

    public void setKwna(String kwna) {
        this.kwna = kwna;
    }

    public String getPekerjaana() {
        return pekerjaana;
    }

    public void setPekerjaana(String pekerjaana) {
        this.pekerjaana = pekerjaana;
    }

    public String getAlamata() {
        return alamata;
    }

    public void setAlamata(String alamata) {
        this.alamata = alamata;
    }

    public String getProvinsia() {
        return provinsia;
    }

    public void setProvinsia(String provinsia) {
        this.provinsia = provinsia;
    }

    public String getKecamatana() {
        return kecamatana;
    }

    public void setKecamatana(String kecamatana) {
        this.kecamatana = kecamatana;
    }

    public String getKabupatena() {
        return kabupatena;
    }

    public void setKabupatena(String kabupatena) {
        this.kabupatena = kabupatena;
    }

    public String getKelurahana() {
        return kelurahana;
    }

    public void setKelurahana(String kelurahana) {
        this.kelurahana = kelurahana;
    }

    public String getNamai() {
        return namai;
    }

    public void setNamai(String namai) {
        this.namai = namai;
    }

    public String getUmuri() {
        return umuri;
    }

    public void setUmuri(String umuri) {
        this.umuri = umuri;
    }

    public String getAgamai() {
        return agamai;
    }

    public void setAgamai(String agamai) {
        this.agamai = agamai;
    }

    public String getKwni() {
        return kwni;
    }

    public void setKwni(String kwni) {
        this.kwni = kwni;
    }

    public String getPekerjaani() {
        return pekerjaani;
    }

    public void setPekerjaani(String pekerjaani) {
        this.pekerjaani = pekerjaani;
    }

    public String getAlamati() {
        return alamati;
    }

    public void setAlamati(String alamati) {
        this.alamati = alamati;
    }

    public String getProvinsii() {
        return provinsii;
    }

    public void setProvinsii(String provinsii) {
        this.provinsii = provinsii;
    }

    public String getKecamatani() {
        return kecamatani;
    }

    public void setKecamatani(String kecamatani) {
        this.kecamatani = kecamatani;
    }

    public String getKabupateni() {
        return kabupateni;
    }

    public void setKabupateni(String kabupateni) {
        this.kabupateni = kabupateni;
    }

    public String getKelurahani() {
        return kelurahani;
    }

    public void setKelurahani(String kelurahani) {
        this.kelurahani = kelurahani;
    }
    
    
}
