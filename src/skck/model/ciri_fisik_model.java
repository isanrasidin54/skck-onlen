/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;


import java.sql.*;
import skck.koneksi.koneksi;
import skck.view.HOME;
import skck.model.hubungan_keluarga_model;
/**
 *
 * @author acer
 */
public class ciri_fisik_model {
    
    String rambut,wajah,kulit,tbadan,bbadan,tistimewa,sjarir,sjaril;

    public String getRambut() {
        return rambut;
    }

    public void setRabmut(String ramut) {
        this.rambut = rambut;
    }

    public String getWajah() {
        return wajah;
    }

    public void setWajah(String wajah) {
        this.wajah = wajah;
    }

    public String getKulit() {
        return kulit;
    }

    public void setKulit(String kulit) {
        this.kulit = kulit;
    }

    public String getTbadan() {
        return tbadan;
    }

    public void setTbadan(String tbadan) {
        this.tbadan = tbadan;
    }

    public String getBbadan() {
        return bbadan;
    }

    public void setBbadan(String bbadan) {
        this.bbadan = bbadan;
    }

    public String getTistimewa() {
        return tistimewa;
    }

    public void setTistimewa(String tistimewa) {
        this.tistimewa = tistimewa;
    }

    public String getSjarir() {
        return sjarir;
    }

    public void setSjarir(String sjarir) {
        this.sjarir = sjarir;
    }

    public String getSjaril() {
        return sjaril;
    }

    public void setSjaril(String sjaril) {
        this.sjaril = sjaril;
    }
    
    
}
