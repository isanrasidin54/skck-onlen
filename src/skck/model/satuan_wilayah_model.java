/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.model;

/**
 *
 * @author acer
 */
public class satuan_wilayah_model {
    
    String jpekerjaan,wilayah,alamatprovinsi,kabupaten,kecamatan,keseluruhan,bayar,rekening;

    public String getJpekerjaan() {
        return jpekerjaan;
    }

    public void setJpekerjaan(String jpekerjaan) {
        this.jpekerjaan = jpekerjaan;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String getAlamatprovinsi() {
        return alamatprovinsi;
    }

    public void setAlamatprovinsi(String alamatprovinsi) {
        this.alamatprovinsi = alamatprovinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKeseluruhan() {
        return keseluruhan;
    }

    public void setKeseluruhan(String keseluruhan) {
        this.keseluruhan = keseluruhan;
    }

    public String getBayar() {
        return bayar;
    }

    public void setBayar(String bayar) {
        this.bayar = bayar;
    }

    public String getRekening() {
        return rekening;
    }

    public void setRekening(String rekening) {
        this.rekening = rekening;
    }
    
    
}
