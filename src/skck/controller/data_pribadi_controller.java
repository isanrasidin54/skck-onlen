/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import java.sql.*;
import skck.koneksi.koneksi;
import skck.view.HOME;
import skck.model.data_pribadi_model;
import javax.swing.JOptionPane;

/**
 *
 * @author acer
 */

public class data_pribadi_controller {

    private koneksi db;
    private data_pribadi_model model;
    private HOME views;
    
    public data_pribadi_model getdata_pribadi_model() {
        return model;
    }

    public void setdata_pribadi_model(data_pribadi_model data_pribadi_model) {
        this.model = model;
    }
    
    public data_pribadi_controller(HOME views){
        this.views = views;
    }
    
    public void simpanForm() {
        model = new data_pribadi_model();
        model.setNama(views.getNamaDP().getText());
        model.setTlahir(views.getTempatDP().getText());
        model.setTgl(views.getTanggalDP().getText());
        if(views.getLakiDP().isSelected() && !views.getPerempuanDP().isSelected()){
           model.setJenisk("Laki-Laki");
        }else{
           model.setJenisk("Perempuan");
        }
        if(views.getKawinDP().isSelected() && !views.getBelumDP().isSelected()){
           model.setKwn("Kawin");
        }else{
           model.setKwn("Belum Kawin");
        }
        if(views.getWniDP().isSelected() && !views.getWnaDP().isSelected()){
           model.setStatus("WNI");
        }else{
           model.setStatus("WNA");
        }
        model.setAgama((String)views.getAgamaDP().getSelectedItem());
        model.setPekerjaan(views.getPekerjaanDP().getText());
        model.setNotlp(views.getNoTelpDP().getText());
        model.setAlamat(views.getAlamatDP().getText());
        model.setProvinsi((String)views.getProvinsiDP().getSelectedItem());
        model.setKabupaten((String)views.getKabupatenDP().getSelectedItem());
        model.setKecamatan((String)views.getKecamatanDP().getSelectedItem());
        model.setKelurahan((String)views.getKelurahanDP().getSelectedItem());
        model.setTipeidentitas((String)views.getTipeIdentitasDP().getSelectedItem());
        model.setNoidentitas(views.getNoIdentitasDP().getText());
        
        String nama             = views.getNamaDP().getText();
        String tempat           = views.getTempatDP().getText();
        String tanggal          = views.getTanggalDP().getText();
        String agama            = (String)views.getAgamaDP().getSelectedItem();
        String pekerjaan        = views.getPekerjaanDP().getText();
        String NoTelp           = views.getNoTelpDP().getText();
        String Alamat           = views.getAlamatDP().getText();
        String Provinsi         = (String)views.getProvinsiDP().getSelectedItem();
        String Kabupaten        = (String)views.getKabupatenDP().getSelectedItem();
        String Kecamatan        = (String)views.getKecamatanDP().getSelectedItem();
        String Kelurahan        = (String)views.getKelurahanDP().getSelectedItem();
        String TipeIdentitas    = (String)views.getTipeIdentitasDP().getSelectedItem();
        String NoIdentitas      = views.getNoIdentitasDP().getText();
        //JOptionPane.showMessageDialog(views,model.getNama()+"\n"+model.getTlahir()+"\n"+model.getTgl()+"\n"+model.getJenisk()+"\n"+model.getKwn()+"\n"+model.getStatus());
        Insert(nama,tempat,tanggal,model.getStatus(),model.getJenisk(),model.getStatus(),agama,pekerjaan,NoTelp,Alamat,Provinsi,Kabupaten,Kecamatan,Kelurahan,TipeIdentitas,NoIdentitas);
    }
    
    public boolean Insert(String nama,String tempat,String tanggal,String kelamin,String kawin,String status, String agama, String pekerjaan , String NoTelp , String Alamat , String Provinsi , String Kabupaten , String Kecamatan , String Kelurahan , String TipeIdentitas , String NoIdentitas)
    {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO datpri VALUES('','" + nama + "','" + tempat + "','" + tanggal + "','" + kelamin + "','" + kawin + "','" + status + "','" + agama + "','" + pekerjaan + "','" + NoTelp + "','" + Alamat + "','" + Provinsi + "','" + Kabupaten + "','" + Kecamatan + "','" + Kelurahan + "','" + TipeIdentitas + "','" + NoIdentitas + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}




