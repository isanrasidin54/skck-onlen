/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;
import java.sql.*;
import skck.koneksi.koneksi;
import skck.model.ciri_fisik_model;
import skck.view.HOME;
import skck.model.keterangan_model;
/**
 *
 * @author acer
 */
public class keterangan_controller {
    private koneksi db;
    private keterangan_model model;
    private HOME views;
    
    public keterangan_model getketerangan_model() {
    return model;
    }
    public void setketerangan_model(keterangan_model keterangan_model) {
        this.model = model;
    }
    public keterangan_controller(HOME views){
        this.views = views;
    }
    
    public void simpanForm() {
        model = new keterangan_model();
        model.setRpekerjaan(views.getRiwayatKT().getText());
        model.setHobi(views.getHobiKT().getText());
        model.setAlamat(views.getAlamatKT().getText());
        model.setSponsor(views.getSponsorKT().getText());
        model.setAlamatsp(views.getAlamatSponsorKT().getText());
        model.setTelp(views.getTeleponKT().getText());
        model.setJenis(views.getJenisUsaha().getText());
        model.setEmail(views.getAlamatEmailKT().getText());
        
        String rpekerjaan             = views.getRiwayatKT().getText();
        String hobi           = views.getHobiKT().getText();
        String alamat          = views.getAlamatKT().getText();
        String sponsor            =  views.getSponsorKT().getText();
        String alamatsp        = views.getAlamatSponsorKT().getText();
        String telp        = views.getTeleponKT().getText();
        String jenis        = views.getJenisUsaha().getText();
        String email        = views.getAlamatEmailKT().getText();
        
        //JOptionPane.showMessageDialog(views,model.getNama()+"\n"+model.getTlahir()+"\n"+model.getTgl()+"\n"+model.getJenisk()+"\n"+model.getKwn()+"\n"+model.getStatus());
        Insert(rpekerjaan, hobi, alamat, sponsor, alamatsp, telp,jenis, email);
    }

    public boolean Insert(String rpekerjaan,String hobi,String alamat,String sponsor,String alamatsp,String telp, String jenis , String email)
    {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO keterangan VALUES('','" + rpekerjaan + "','" + hobi + "','" + alamat + "','" + sponsor + "','" + alamatsp + "','" + telp + "','" + jenis + "','" + email + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
