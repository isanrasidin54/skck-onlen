/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import java.sql.*;
import skck.koneksi.koneksi;
import skck.view.HOME;
import skck.model.satuan_wilayah_model;
/**
 *
 * @author acer
 */
public class satuan_wilayah_controller {
    private koneksi db;
    private satuan_wilayah_model model;
    private HOME views;
    
    public void setsatuan_wilayah_model(satuan_wilayah_model satuan_wilayah_model) {
        this.model = model;
    }
    
    public satuan_wilayah_controller(HOME views){
        this.views = views;
    }
    
    public void simpanForm() {
        model = new satuan_wilayah_model();
        
        if(views.getTunaiSW().isSelected() && !views.getBrivaSW().isSelected()){
           model.setBayar("Tunai");
        }else{
           model.setBayar("BRIVA");
        }
        
        String Tujuan           = (String)views.getKeperluanSW().getSelectedItem();
        String Kesatuan         = (String)views.getKesatuanSW().getSelectedItem();
        String Tingkat          = (String)views.getTingkatSW().getSelectedItem();
        String Alamat           = views.getAlamatSW().getText();
        String Provinsi         = (String)views.getProvinsiSW().getSelectedItem();
        String Kabupaten        = (String)views.getKabupatenSW().getSelectedItem();
        String Kecamatan        = (String)views.getKecamatanSW().getSelectedItem();
        String Kelurahan        = (String)views.getKelurahanSW().getSelectedItem();
        String NoRek            = views.getNoRekSW().getText();
        
        Insert(Tujuan,Kesatuan,Tingkat,Alamat,Provinsi,Kabupaten,Kecamatan,Kelurahan,model.getBayar(),NoRek);
    }
    
    public boolean Insert(String Pekerjaan ,String Wilayah ,String Satuan , String Alamat ,String Provinsi ,String Kabupaten ,String Kecamatan, String Kelurahan, String Bayar , String Rekening){
            
        boolean result = false;
        Statement stmt = db.createStmt();
       String query = "INSERT INTO satwil VALUES('','" + Pekerjaan + "','" + Wilayah + "','" + Satuan + "','" + Alamat + "','" + Provinsi + "','" + Kabupaten + "','" + Kecamatan + "','" + Kelurahan + "','" + Bayar + "','" + Rekening + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
