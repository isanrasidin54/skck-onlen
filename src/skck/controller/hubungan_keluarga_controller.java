/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import java.sql.*;
import skck.koneksi.koneksi;
import skck.view.HOME;
import skck.model.hubungan_keluarga_model;
/**
 *
 * @author acer
 */
public class hubungan_keluarga_controller {
    private koneksi db;
    private hubungan_keluarga_model model;
    private HOME views;
    
    public void sethubungan_keluarga_model(hubungan_keluarga_model hubungan_keluarga_model) {
        this.model = model;
    }
    
    public hubungan_keluarga_controller(HOME views){
        this.views = views;
    }
    
    
    
    public void simpanForm(){
        model = new hubungan_keluarga_model();
         if(views.getWniHK().isSelected() && !views.getWnaHK().isSelected()){
           model.setKwnp("WNI");
        }else{
           model.setKwnp("WNA");
        }
        
        if(views.getWniAHK().isSelected() && !views.getWnaAHK().isSelected()){
           model.setKwna("WNI");
        }else{
           model.setKwna("WNA");
        }
        
        if(views.getWniIHK().isSelected() && !views.getWnaIHK().isSelected()){
           model.setKwni("WNI");
        }else{
          model.setKwni("WNA");
        }
        
        String hubunganP        = (String)views.getHubunganHK().getSelectedItem();
        String AgamaP           = (String)views.getAgamaHK().getSelectedItem();
        String AlamatP          = views.getNamaHK().getText();
        String NamaP            = views.getAlamatHK().getText();
        String PekerjaanP       = views.getPekerjaanHK().getText();;
        String ProvinsiP        = (String)views.getProvinsiHK().getSelectedItem();
        String KabupatenP       = (String)views.getKabupatenHK().getSelectedItem();
        String KecamatanP       = (String)views.getKecamatanHK().getSelectedItem();
        String KelurahanP       = (String)views.getKelurahanHK().getSelectedItem();
        String UmurP            = views.getUmurHK().getText();
        String AgamaA           = (String)views.getAgamaAHK().getSelectedItem();
        String AlamatA          = views.getNamaAHK().getText();
        String NamaA            = views.getAlamatAHK().getText();
        String PekerjaanA       = views.getPekerjaanAHK().getText();;
        String ProvinsiA        = (String)views.getProvinsiAHK().getSelectedItem();
        String KabupatenA       = (String)views.getKabupatenAHK().getSelectedItem();
        String KecamatanA       = (String)views.getKecamatanAHK().getSelectedItem();
        String KelurahanA       = (String)views.getKelurahanAHK().getSelectedItem();
        String UmurA            = views.getUmurAHK().getText();
        String AgamaI           = (String)views.getAgamaIHK().getSelectedItem();
        String AlamatI          = views.getNamaIHK().getText();
        String NamaI            = views.getAlamatIHK().getText();
        String PekerjaanI       = views.getPekerjaanIHK().getText();;
        String ProvinsiI        = (String)views.getProvinsiIHK().getSelectedItem();
        String KabupatenI       = (String)views.getKabupatenIHK().getSelectedItem();
        String KecamatanI       = (String)views.getKecamatanIHK().getSelectedItem();
        String KelurahanI       = (String)views.getKelurahanIHK().getSelectedItem();
        String UmurI            = views.getUmurIHK().getText();
        
        Insert(hubunganP,NamaP,UmurP,AgamaP,model.getKwnp(),PekerjaanP,AlamatP,ProvinsiP,KabupatenP,KecamatanP,KelurahanP,NamaA,UmurA,AgamaA,model.getKwna(),PekerjaanA,AlamatA,ProvinsiA,KabupatenA,KecamatanA,KelurahanA,NamaI,UmurI,AgamaI,model.getKwni(),PekerjaanI,AlamatI,ProvinsiI,KabupatenI,KecamatanI,KelurahanI);
            
    }
    
    public boolean Insert(String Hubungan , String Nama ,String Umur ,String Agama, String Kewarganegaraan , String Pekerjaan , String Alamat, String Provinsi , String Kabupaten ,String Kecamatan ,  String Kelurahan,String NamaA ,String UmurA ,String AgamaA, String KewarganegaraanA , String PekerjaanA , String AlamatA, String ProvinsiA , String KabupatenA ,String KecamatanA ,  String KelurahanA,String NamaI ,String UmurI, String AgamaI, String KewarganegaraanI , String PekerjaanI , String AlamatI, String ProvinsiI , String KabupatenI ,String KecamatanI ,  String KelurahanI)
    {     
        boolean result = false;
        Statement stmt = db.createStmt();
       String query = "INSERT INTO hubkel VALUES('','"+Hubungan+"','"+Nama+"','"+Umur+"','"+Agama+"','"+Kewarganegaraan+"','"+Pekerjaan+"','"+Alamat+"','"+Provinsi+"','"+Kabupaten+"','"+Kecamatan+"','"+Kelurahan+"','"+NamaA+"','"+UmurA+"','"+AgamaA+"','"+KewarganegaraanA+"','"+PekerjaanA+"','"+AlamatA+"','"+ProvinsiA+"','"+KabupatenA+"','"+KecamatanA+"','"+KelurahanA+"','"+NamaI+"','"+UmurI+"','"+AgamaI+"','"+KewarganegaraanI+"','"+PekerjaanI+"','"+AlamatI+"','"+ProvinsiI+"','"+KabupatenI+"','"+KecamatanI+"','"+KelurahanI+"')";
        
        
        try{
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
