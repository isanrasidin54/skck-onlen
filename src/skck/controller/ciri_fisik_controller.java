/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.controller;

import java.sql.*;
import skck.koneksi.koneksi;
import skck.view.HOME;
import skck.model.ciri_fisik_model;
/**
 *
 * @author acer
 */
public class ciri_fisik_controller {
    private koneksi db;
    private ciri_fisik_model model;
    private HOME views;
    
    public ciri_fisik_model getciri_fisik_model() {
    return model;
    }
    public void setciri_fisik_model(ciri_fisik_model ciri_fisik_model) {
        this.model = model;
    }
    
    public ciri_fisik_controller(HOME views){
        this.views = views;
    }
    public void simpanForm() {
        model = new ciri_fisik_model();
        model.setRabmut(views.getRambutCF().getText());
        model.setWajah(views.getWajahCF().getText());
        model.setKulit(views.getKulitCF().getText());
        model.setTbadan(views.getTInggiCF().getText());
        model.setBbadan(views.getBeratCF().getText());
        model.setTistimewa(views.getTandaCF().getText());
        model.setSjarir(views.getJariKiriCF().getText());
        model.setSjaril(views.getJariKananCF().getText());
        
        
        String rambut             = views.getRambutCF().getText();
        String wajah           = views.getWajahCF().getText();
        String kulit          = views.getKulitCF().getText();
        String tbadan            =  views.getTInggiCF().getText();
        String bbadan        = views.getBeratCF().getText();
        String tistimewa        = views.getTandaCF().getText();
        String sjarir        = views.getJariKiriCF().getText();
        String sjaril        = views.getJariKananCF().getText();
        
        //JOptionPane.showMessageDialog(views,model.getNama()+"\n"+model.getTlahir()+"\n"+model.getTgl()+"\n"+model.getJenisk()+"\n"+model.getKwn()+"\n"+model.getStatus());
        Insert(rambut, wajah, kulit, tbadan, bbadan, tistimewa,sjarir, sjaril);
    }
    
    public boolean Insert(String rambut,String wajah,String kulit,String tbadan,String bbadan,String tistimewa, String sjarir , String sjaril)
    {
        boolean result = false;
        Statement stmt = db.createStmt();
        String query = "INSERT INTO cirifisik VALUES('','" + rambut + "','" + wajah + "','" + kulit + "','" + tbadan + "','" + bbadan + "','" + tistimewa + "','" + sjarir + "','" + sjaril + "')";
        try {
            stmt.execute(query);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
